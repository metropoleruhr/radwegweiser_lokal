## Ein QGIS-Plugin für ein Radwegweisungskataster

Die [Hinweise zur wegweisenden Beschilderung für den Radverkehr in NRW (HBR NRW)](https://www.radverkehrsnetz.nrw.de/rvn_hbr.asp) 
geben technische Hinweise für eine einheitliche Beschilderung des Radnetz-NRW. 

Der Regionalverband Ruhr hat für die Dokumentation der von ihm unterhaltenen Wegweiser eine Software auf Basis
des freien Geoinformationssystems QGIS entwickelt. Auf vielfachen Wunsch im Geonetzwerk.metropoleRuhr wurde
diese Software soweit modifiziert, dass sie als Einzelplatz-Lösung ohne weitere Backend-Voraussetzungen 
(z.B. Datenbankserver) nutzbar ist.

Dies hat folgende Vorteile:
* die Kommunen können ihr radwegweisende Beschilderung ohne weitere Softwarekosten HBR NRW-konform dokumentieren
* PDF-Katasterblätter können so einfach erzeugt und dem landesweiten Kataster zur Verfügung gestellt werden
* die Datenbankstruktur und Software sind quelloffen und anpassbar.

Das Plugin **Radwegweiser (lokal)** hat u.a. folgende Funktionen:
* Verwaltung von Knoten, Pfosten, Wegweisern und detaillierten Schildinhalten in übersichtlichen Bedienoberflächen
* Kartenlayer der Knoten, Pfosten, Wegweiser zur Visualisierung und Auswahl zu bearbeitender Objekte 
* Fotodokumentation des Wegweiserbestandes
* Erzeugung der Wegweisergrafik mittels QML (Qt Modeling Language) und QtQuick
* Speicherung der Grafikdateien zu Wegweisern, Knotenpunkt- und Routeneinschüben sowie Fotos im lokalen Dateisystem
* Datenhaltung in einer SpatiaLite-Geodatenbank (GeoPackage)
* Erzeugung eines PDF-Katasterblattes zu allen Pfosten eines Knotens


### Voraussetzungen

Das Plugin verwendet das Plugin Build Tool:
https://g-sherman.github.io/plugin_build_tool/

Installieren mit:
```
pip install pb_tool
```

Für die HTML-Hilfe wird ein Theme verwendet:
```
pip install sphinx_rtd_theme
```

Den Plugin-Code lokal bereitstellen:
```
git clone https://gitlab.opencode.de/metropoleruhr/radwegweiser_lokal.git
```
In pb_tool.cfg den plugin_path anpassen, dann das Plugin erstellen mit:
```
pbt deploy -y
```

Die HTML-Hilfe findet sich dann unter help/build/html/ .
