
from PyQt5 import QtSql
from PyQt5.QtCore import Qt, QRect, QEvent, QObject, QSettings, QFileInfo, QThread, QSortFilterProxyModel, QCoreApplication
from PyQt5.QtWidgets import QApplication, QComboBox, QCompleter, QHeaderView, QAction, QDataWidgetMapper, \
    QFileDialog, QPushButton, QStyleOptionButton, QStyle, QItemDelegate, QStyledItemDelegate, QMessageBox, QShortcut 
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QColor, QIcon, QKeySequence

class CheckBoxDelegate(QStyledItemDelegate):
    """
    A delegate that places a fully functioning QCheckBox in every
    cell of the column to which it's applied
    """
    def __init__(self, parent=None):
        QItemDelegate.__init__(self, parent)
 
    def createEditor(self, parent, option, index):
        '''
        Important, otherwise an editor is created if the user clicks in this cell.
        ** Need to hook up a signal to the model
        '''
        return None
 
    def paint(self, painter, option, index):
        '''
        Paint a checkbox without the label.
        '''
        checked = index.model().data(index, Qt.DisplayRole)
        check_box_style_option = QStyleOptionButton()
 
        if (index.flags() & Qt.ItemIsEditable) > 0:
            check_box_style_option.state |= QStyle.State_Enabled
        else:
            check_box_style_option.state |= QStyle.State_ReadOnly
 
        if checked:
            check_box_style_option.state |= QStyle.State_On
        else:
            check_box_style_option.state |= QStyle.State_Off
 
        check_box_style_option.rect = self.getCheckBoxRect(option)
            
        check_box_style_option.state |= QStyle.State_Enabled
 
        QtGui.QApplication.style().drawControl(QStyle.CE_CheckBox, check_box_style_option, painter)
 
    def editorEvent(self, event, model, option, index):
        '''
        Change the data in the model and the state of the checkbox
        if the user presses the left mousebutton or presses
        Key_Space or Key_Select and this cell is editable. Otherwise do nothing.
        '''
#         print 'Check Box editor Event detected : ' 
#         if not (index.flags() & QtCore.Qt.ItemIsEditable) > 0:
#             return False
            
        # print 'Check Box edior Event detected : passed first check' 
        # Do not change the checkbox-state
        if event.type() == QEvent.MouseButtonRelease:
            if event.button() == Qt.LeftButton and self.getCheckBoxRect(option).contains(event.pos()):
                # print 'left und treffer'
                self.setModelData(None, model, index)
                return True
        else:
            return False
 
        return True
 
    def setModelData (self, editor, model, index):
        'The user wanted to change the old state in the opposite.'
        # Toggle checked-State
        newValue = True if index.model().data(index, Qt.EditRole) != True else False
        # print 'New Value : {0}'.format(newValue)
        model.setData(index, newValue, Qt.EditRole)
 
    def getCheckBoxRect(self, option):
        check_box_style_option = QStyleOptionButton()
        check_box_rect = QApplication.style().subElementRect(QStyle.SE_CheckBoxIndicator, check_box_style_option, None)
        check_box_point = QPoint (option.rect.x() + 
                             option.rect.width() / 2 - 
                             check_box_rect.width() / 2,
                             option.rect.y() + 
                             option.rect.height() / 2 - 
                             check_box_rect.height() / 2)
        return QRect(check_box_point, check_box_rect.size())
    
    