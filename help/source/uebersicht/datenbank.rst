.. _datenbank_uebersicht:

Datenbank
################################################

Es wird ein GeoPackage (SQLite-Datenbank) im lokalen Dateisystem eingesetzt. |br|
Diese Geodatenbank verwaltet die Tabellen und Sichten, die zur Verortung der Knoten 
und Pfosten sowie der Erzeugung der Wegweiser-Grafik erforderlich sind.  

.. _tab_uebersicht_tabellen:

.. list-table:: Übersicht der Tabellen 
   :widths: 23 50
   :header-rows: 1

   * - Tabelle
     - Beschreibung
   * - einschub
     - Katalog zu Einschüben an Wegweisern |br|
       (Hinweis auf Routen oder Knotenpunkte)
   * - filter
     - vordefinierte Filter (zum Filtern von Knoten bzw. Pfosten)
   * - foto
     - Hochgeladene Fotodateien zu Pfosten und Wegweisern
   * - foto_wegweiser_assoc
     - Zuordnungstabelle (m:n) Foto -> Wegweiser
   * - gruppe
     - Anwendergruppen, die bestimmte Rechte |br| bei zugeordneten Pfosten haben.
   * - inhalt
     - Schildinhalte (z.B. Zielname, Entfernung) auf Wegweisern
   * - knoten
     - Knoten gruppieren mehrere Pfosten
   * - pfeilart
     - Katalog zu Pfeilarten (z.B. schräg links) |br| und Zuordnung des Zeichens im TrueType-Font.
   * - pfosten
     - Standort eines Pfostens
   * - pfosten_eigentuemer
     - Katalog zu Pfosten-Eigentümerkategorien
   * - pfosten_gruppe_assoc
     - Zuordnungstabelle (m:n) Pfosten -> Gruppen
   * - pfosten_material
     - Katalog zu Pfostenmaterialien
   * - pfosten_status
     - Katalog zu Pfosten-Status |br| z.B. geprüft, in Prüfung, Ortstermin
   * - pfosten_typ
     - Katalog zu Pfostentypen
   * - richtung
     - Katalog zu Richtungen (z.B. Nordost) mit |br| Beschriftung für Pfeil- bzw. Tabellenwegweiser
   * - streckenpiktogramm
     - Katalog zu Streckenpiktogrammen |br| (z.B. Freizeitstrecke, Bahntrassenradweg)
   * - wegweiser
     - Wegweiser an einem Pfosten
   * - wegweiser_einschub_assoc
     - Zuordnungstabelle (m:n) Wegweiser -> Einschübe
   * - wegweiser_typ
     - Katalog zu Wegweisertypen |br| (z.B. Pfeil-, Tabellen- oder Zwischenwegweiser)
   * - zielpiktogramm
     - Katalog zu Zielpiktogrammen |br| (z.B. Bahnhof, Fähre, Radstation)


.. _tab_uebersicht_views1:

.. list-table:: Übersicht der Sichten (1)
   :widths: 15 50
   :header-rows: 1

   * - Datenbanksicht
     - Beschreibung
   * - foto_wegweiser
     - Fotos mit Wegweiser-ID ausgeben |br| (zur Anzeige der Fotos je Pfosten/Wegweiser)
   * - pfeilart_view
     - Nur anzuzeigende Pfeilarten ausgeben
   * - pfosten_gruppen
     - Einem Pfosten zugeordnete Gruppen |br| und deren Rechte zur Bearbeitung ausgeben
   * - pfostenfoto_view
     - Je Pfosten ein HTML mit Verweis auf die |br| zugeordneten Fotos nebst Fototitel ausgeben

.. _tab_uebersicht_views2:

.. list-table:: Übersicht der Sichten (Fortsetzung)
   :widths: 15 50
   :header-rows: 1

   * - Datenbanksicht
     - Beschreibung
   * - pfostenhtml_view
     - Je Pfosten ein HTML mit Verweis auf die Schildergrafiken |br| (und ggf. Knotenpunkthut) ausgeben
   * - pfostenpdf_view
     - Pfosten-Layer mit Schilder- bzw. Foto-HTML |br| 
       (dient als sog. Abdeckungslayer im Layout-Atlas)
   * - wegweiser_einschub
     - Einschübe mit Wegweiser-ID ausgeben |br| (zur Anzeige der Einschübe je Wegweiser)
   * - wegweiser_view
     - Wegweiser-Layer mit Pfosten-Standort, Richtung/Drehung |br| 
       (zur Anzeige der Wegweiser in Karte und Layout-Atlas (PDF))

Für die Tabellen knoten, pfosten und wegweiser sind (BEFORE DELETE) Trigger eingerichtet, 
die dafür sorgen, dass andere Tabellen nach dem Löschen eines dieser Objekte keine fehlerhaften Verweise
auf gelöschte Datensätze haben.


