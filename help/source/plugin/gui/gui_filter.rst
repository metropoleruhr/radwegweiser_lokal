.. _gui_filter:


Reiter Filter
********************

Um bestimmte Pfosten (und Knoten) schnell zu finden, enthält das Plugin eine generische Filterfunktion.
Diese erlaubt es für wiederkehrende Aufgabenstellungen parametrisierte Filter
zu konfigurieren. |br|
Ein Filter besteht aus:

* einer Bezeichnung, 
* einer ausführlichen Beschreibung, die die Filterkriterien (Parameter) und die abgefragten Pfosten beschreibt, 
* und einer Filter-Abfrage (SQL-Ausdruck), die Konfigurationen für die Parameter enthält.

.. figure:: img/filtern2.png
    :figclass: align-center
    :width: 70%

|np| 

Das Ergebnis der Abfrage sind ID-Werte für Pfosten (und/oder Knoten). 
Diese werden im SQL-Ausdruck als Feld "pfosten_id" (bzw. knoten_id) definiert. 
Die Tabelle der Pfosten bzw. Knoten wird auf diese ID-Werte gefiltert.

Der Pfosten-Reiter zeigt dann die gefilterte Anzahl der Pfosten an.
Durch erneutes Klicken auf |I1| wird der Filter deaktiviert und in der Tabelle aufgehoben.

Die Bedienoberfläche besteht aus einer Tabelle der definierten Filter sowie einem Formular
zur Bearbeitung der Daten des aktuell gewählten Filters.

Unterhalb der Tabelle der Filter befinden sich die folgenden Schaltflächen:

* |I2| ermöglicht das Hinzufügen eines neuen Filters. Es öffnet sich ein Dialog zur Eingabe einer Bezeichnung für den Filter.
* |I3| erlaubt, das Löschen eines Filters. Es erfolgt eine Sicherheitsabfrage. 
* |I4| Änderungen am Filter speichern. Wird aktiv, wenn z.B. Name, Beschreibung usw. geändert wurden. 
* |I5| Änderungen am Filter verwerfen oder Formular neu füllen.
    
Unterhalb des Formulars finden sich die Bedienelemente zum Ausführen der Filterung.

Immer präsent ist die Schaltfläche |I1| **Filter aktivieren** sie wird bei einem aktiven Filter
hellrot hinterlegt. Der Filter kann durch erneutes Klicken der Schaltfläche deaktiviert werden.
Die eingegebenen Parameter bleiben auch nach dem Deaktivieren des Filters erhalten und werden
erst bei Wechsel zu einem anderen Filter (oder **Strg-Klick auf** |I1|) verworfen.

Die Bedienelemente zur Eingabe der Filterbedingungen (Parameter) werden dynamisch nach Auswahl 
eines Filters erzeugt. Es werden folgende Elemente unterstützt:

.. figure:: img/filter_combobox.png
    :figclass: align-center

Ein **Kombinationsfeld** zur Auswahl von Nachschlagewerten (z.B. Schlagworte).
Je nach Einstellung (multi vs. single) kann ein oder mehrere Nachschlagewerte ausgewählt werden.
Bei Mehrfachauswahl wird unterhalb des Kombinationsfeldes die aktuelle Auswahl angezeigt.

.. important:: Sie müssen aktiv einen Wert aus dem Kombinationsfeld auswählen, um die Filterbedingung zu setzen.
    Ansonsten scheitert die Filterung.

.. figure:: img/filter_line.png
    :figclass: align-center

Eine **Textzeile** zur Eingabe von Buchstaben oder Ziffern. Es kann ein einfacher regulärer Ausdruck 
konfiguriert werden, um die Eingabe auf z.B. nur Ziffern zu begrenzen.

.. figure:: img/filter_checkbox.png
    :figclass: align-center

Ein **Auswahlkästchen** zur Eingabe von Ja/Nein-Werten.

|np|

.. figure:: img/filter_date.png
    :figclass: align-center

Eine **Datumsauswahl** zur Eingabe von Datumswerten.

.. _filter_point:

.. figure:: img/filter_point.png
    :figclass: align-center

Ein **Punktwerkzeug** zur Definition eines Punktes in der Karte, von dem aus gesucht werden soll.
Hier z.B. alle Pfosten in max. Entfernung von 1500 m. 

Aktivieren Sie das Werkzeug und Klicken Sie in die Karte, um den Punkt zu definieren.
Die Koordinaten des Punktes werden neben dem Werkzeug angezeigt.

Der Punkt wird in einen temporären **Layer "Filter-Geometrie"** eingetragen, damit Sie die
Position visuell nachvollziehen können. 

.. hint:: Der Layer nimmt nacheinander alle positionierten Filter-Punkte auf. Klicken Sie mit
            gedrückter Strg-Taste auf das Punktwerkzeug, um den Layer "Filter-Geometrie" zu leeren.


Filterabfragen konfigurieren
"""""""""""""""""""""""""""""

Die Filterabfrage ist ein SQL-SELECT-Ausdruck der bestimmte Konfigurationen für die Parameter und deren 
Bedienelemente enthält. Diese werden im Folgenden dokumentiert.

.. warning:: Es sind nur SELECT-Abfragen möglich. DELETE, INSERT usw. werden unterbunden. 

Eine typische Parameter-Konfiguration sieht wie folgt aus:

**WHERE g.id IN ([gemeinde|AsIs|katalog_3|gemeinden_nrw|None|multi|Gemeinden])**

je Parameter enthält der SQL-Ausdruck eine in eckigen Klammern eingefasste Zeichenkette,
die aus sieben durch einen senkrechten Strich (Pipe) getrennte Teilen besteht.

|np|

Die Teile bedeuten in ihrer Abfolge 

#. Bezeichnung des Parameters im SQL
#. AsIs = Wert nicht als String übergeben, None = Wert als String übergeben, |br| Part = Wert ist ein Teilstring der mit % (= jedes beliebige Zeichen) eingefasst wird
#. Typ des Eingabelements: |br| 
   katalog_1 = Kombinationsfeld, text = Textzeile, date = Datumsauswahl, boolean = Auswahlkästchen, geom = Punktwerkzeug |br|
   katalog_2 bedeutet, dass das Feld an Indexposition 2 im Kombinationsfeld angezeigt werden soll
#. wenn Typ katalog: Angabe der zu verwendenden Tabelle ansonsten None
#. wenn Typ text: regulärer Ausdruck, der die Eingabe zulässiger Werte einschränkt ansonsten None 
#. wenn mehrere Werte zu dem Parameter eingegeben werden sollen ( .. IN ()): multi ansonsten single
#. Beschriftung des Eingabeelementes 

.. |I1| image:: img/filter.png
    :width: 16pt
.. |I2| image:: img/add.png
    :width: 16pt
.. |I3| image:: img/delete.png
    :width: 16pt
.. |I4| image:: img/save.png
    :width: 16pt
.. |I5| image:: img/refresh.png
    :width: 16pt







