��                          x     a   �  6   �          ?     U  	   \  M   f     �  	   �     �     �     �  �   �  Y   �       !   #  
   E     P     U  D   f     �     �     �     �     �     �     �       �     �  �  x   I  a   �  8   $	     ]	     }	     �	  	   �	  L   �	     �	  	   �	     
     
     0
  �   F
  X        f  )   l  
   �     �     �  D   �     �                    )     5  	   L     V  �   o   
        <span class="build">
          Build
          <a href="%(build_url)s">%(build_id)s</a>.
        </span>
       
        <span class="commit">
          Revision <code>%(commit)s</code>.
        </span>
       &copy; <a href="%(path)s">Copyright</a> %(copyright)s. &copy; Copyright %(copyright)s. About these documents Builds Copyright Created using <a href="http://sphinx-doc.org/">Sphinx</a> %(sphinx_version)s. Docs Downloads Edit on Bitbucket Edit on GitHub Edit on GitLab Erstellt mit <a href="http://sphinx-doc.org/">Sphinx</a> mit Hilfe eines <a href="https://github.com/snide/sphinx_rtd_theme">Layouts</a> bereitgestellt von <a href="https://readthedocs.org">Read the Docs</a> Free document hosting provided by <a href="http://www.readthedocs.org">Read the Docs</a>. Index Last updated on %(last_updated)s. Navigation Next On Read the Docs Please activate JavaScript to enable the search
      functionality. Previous Project Home Search Search Results Search docs Search within %(docstitle)s Versions View page source Your search did not match any documents. Please make sure that all words are spelled correctly and that you've selected enough categories. Project-Id-Version: sphinx_rtd_theme 0.2.4
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2018-03-10 20:54-0400
PO-Revision-Date: 2018-03-10 20:54-0400
Last-Translator: Dennis Wegner <dennis@instant-thinking.de>
Language: de
Language-Team: German Team
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.14.0
 
        <span class="build">
          Build
          <a href="%(build_url)s">%(build_id)s</a>.
        </span>
       
        <span class="commit">
          Revision <code>%(commit)s</code>.
        </span>
       &copy; <a href=\"%(path)s\">Copyright</a> %(copyright)s. &copy; Copyright %(copyright)s. Über diese Dokumente Builds Copyright Erstellt mit <a href="http://sphinx-doc.org/">Sphinx</a> %(sphinx_version)s. Hilfe Downloads Auf Bitbucket bearbeiten Auf GitHub bearbeiten Auf GitLab bearbeiten Erstellt mit <a href="http://sphinx-doc.org/">Sphinx</a> unter Verwendung eines <a href="https://github.com/snide/sphinx_rtd_theme">Themes</a> von <a href="https://readthedocs.org">Read the Docs</a> Dokumentationshosting gratis bei <a href="http://www.readthedocs.org">Read the Docs</a>. Index Zuletzt aktualisiert am %(last_updated)s. Navigation Weiter Auf Read the Docs Bitte Javascript aktivieren um die Suchfunktion
    zu ermöglichen. Zurück Projektseite Suche Suchergebnisse Durchsuchen Suche in %(docstitle)s Versionen Seitenquelltext anzeigen Ihre Suche ergab keine Treffer. Bitte stellen Sie sicher, dass alle Wörter richtig geschrieben sind und Sie genug Kategorien ausgewählt haben. 