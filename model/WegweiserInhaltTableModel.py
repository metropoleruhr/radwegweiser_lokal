# -*- coding: utf-8 -*-

from builtins import range
from qgis.PyQt.QtCore import Qt
from qgis.PyQt import QtSql
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtSql import QSqlDatabase

import json
#import resources

ID, SCHILD_NR, ZEILE, ZIEL, NAHZIEL, ENGSCHRIFT, SCHRIFTHOEHE, HINWEIS, ENTFERNUNG, WEGWEISER_ID, PFOSTEN, KNOTEN, WEGWEISER, \
            ZIELPIKTOGRAMM1_ID, ZIELPIKTOGRAMM2_ID, STRECKENPIKTOGRAMM1_ID, STRECKENPIKTOGRAMM2_ID, LFDNR = list(range(18))

class WegweiserInhaltTableModel(QtSql.QSqlRelationalTableModel):

    def __init__(self, parent=None, db=QSqlDatabase()):
        super(WegweiserInhaltTableModel, self).__init__(parent, db)
        self.LockedColumns = [ID]

    def flags(self, index):
        flags = QtSql.QSqlTableModel.flags(self, index)
        if index.column() not in self.LockedColumns:
            flags |= Qt.ItemIsEditable
        else:
            flags = Qt.ItemIsEnabled | Qt.ItemIsSelectable
        return flags

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return None

        if role == Qt.TextAlignmentRole:
            if index.column() in (ID, SCHILD_NR, ZEILE, ENTFERNUNG, LFDNR):
                return Qt.AlignCenter | Qt.AlignVCenter
            else:
                return Qt.AlignLeft | Qt.AlignVCenter

        if QtSql.QSqlTableModel.data(self, index, role) == None:
            return None
        else:
            return QtSql.QSqlTableModel.data(self, index, role)

    def setData(self, index, value, role=Qt.EditRole):
        if value == '' or value == None:
            QtSql.QSqlTableModel.setData(self, index, None, role)
        else:
            QtSql.QSqlTableModel.setData(self, index, value, role)

        self.dirty = True
        # self.dataChanged.emit(index, index)
        return True
