.. _qgisprojekt_install:

QGIS-Projektvorlage
*************************

Im **Unterverzeichnis python\\plugins\\radwegweiser_lokal** des QGIS-Profilverzeichnisses findet sich die
QGIS-Projektdatei **radwegweiser_vorlage.qgz**. 

.. important::
  Kopieren Sie die Dateien **radwegweiser_vorlage.qgz** und **radwegweiser.gpkg** in ein eigenes 
  Projektverzeichnis außerhalb des QGIS-Profilverzeichnisses.
  Dadurch vermeiden Sie Datenverluste zwischenzeitlich eingepflegter Wegweiserdaten, 
  sollte das Plugin später erneut installiert werden.

Öffnen Sie die kopierte Projektdatei in QGIS, um das Kataster der Radwegweisung zu bearbeiten. 

Diese QGIS-Projektdatei enthält die erforderlichen Layer zur Bearbeitung von Knoten und Pfosten
sowie zur Visualisierung der Wegweiser.

  .. figure:: img/qgis_projekt1.png
      :figclass: align-center
      :width: 400pt
       
      QGIS-Projekt mit vorbereiteten Layern

.. hint:: 
  Die Datenquelle der Layer verweist auf die im gleichen Verzeichnis der QGIS-Projektvorlage liegende
  Geopackage-Datei radwegweiser.gpkg.

Pfeilwegweiser werden mit der Richtung, in die sie zeigen, andere Wegweiser mit der Richtung aus der sie
sichtbar sind visualisiert. 

Eine nähere Beschreibung der QGIS-Projektvorlage finden sie :ref:`hier <qgisprojekt_layer>` .


|nbsp| |br|
|nbsp| |br|
