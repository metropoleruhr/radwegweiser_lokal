.. _gui_knoten:


Reiter Knoten
********************

Der **Reiter Knoten** listet die :ref:`Knoten <begriff_knoten>` auf und ermöglicht die Bearbeitung ihrer Eigenschaften.
Im Regelfall werden die Knoten, die im aktuell sichtbaren Kartenausschnitt liegen aufgelistet.
Durch Filtern können aber auch Knoten mit bestimmten Eigenschaften gefunden und aufgelistet werden.

Die Tabelle der Knoten enthält die Spalten (eindeutige) **ID**, **Knoten-Nummer** und **Bezeichnung** des Knotens.
In dem Formular zur Bearbeitung des gerade ausgewählten Knotens findet sich zusätzlich noch ein Textfeld
zur Eingabe einer längeren Bemerkung. 

  .. figure:: img/reiter_knoten.png
       :figclass: align-center
       :scale: 80%
       
       Reiter *Knoten*

Das Bedienelement **Schnellfilter** oberhalb der Tabelle erlaubt die Eingabe einer Zeichenkette und zeigt die Knoten an,
deren ID, Knoten-Nr., Bezeichnung oder Bemerkung diese Zeichenkette enthalten.
Es müssen mindestens 3 Zeichen eingegeben sein, damit die Filterung aktiviert wird. |br|
Mit der Schaltfläche |I1| heben Sie die Filterung komplett auf.

Unterhalb der Tabelle der Knoten befinden sich die folgenden Schaltflächen:

* |I2| zoomt den Kartenausschnitt auf die in der Tabelle ausgewählten Knoten (mit 30m Puffer)
* Schaltfläche **Knoten editieren** setzt den Layer Knoten in den Editiermodus und hebt Spatialite-Zugriffssperren auf  
* |I3| löscht den ausgewählten Knoten nach Bestätigung einer Sicherheitsabfrage |br|
  (Die Knoten-ID zugeordneter Pfosten wird zurückgesetzt.)
* |I4| Änderungen an Eigenschaften des Knotens speichern. |br| 
  Wird aktiv, wenn z.B. Knoten-Nr., Beschreibung usw. geändert wurden. 

    .. note:: Die Änderung wird erst erkannt, wenn das Bedienelement verlassen wurde, also den "Fokus" verloren hat.
* |I5| Änderungen an Eigenschaften des Knotens verwerfen und Formular neu füllen. |br|
  (Dabei werden die Knoten im aktuellen Kartenfenster geladen.)

.. |I1| image:: img/filter_aufheben.png
.. |I2| image:: img/zoom_route.png
.. |I3| image:: img/delete.png
.. |I4| image:: img/save.png
.. |I5| image:: img/refresh.png

.. hint:: Doppelklick auf einen Knoten filtert die Tabelle der Pfosten auf diesen Knoten 
          und aktiviert den Reiter Pfosten.
