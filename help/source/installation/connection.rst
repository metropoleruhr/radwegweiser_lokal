.. _dbconnection:

Datenbankverbindung einrichten
#################################

Das QGIS-Plugin Radwegweiser benötigt eine SpatiaLite-Datenbankverbindung, um auf Wegweisungsdaten zuzugreifen
und die Bedienoberfläche mit Daten zu füllen.

Öffnen Sie über das Browser-Bedienfeld das Kontextmenü (rechte Maustaste) "**Neue Verbindung**..." zu **GeoPackage**.
(GeoPackage ist ein SpatiaLite ähnliches dateibasiertes Datenbankformat.)

.. figure:: img/new_db_connection1.png
    :width: 250pt
    :figclass: align-left

Es öffnet sich der Dialog "Geopackage öffnen". 
Wählen Sie hier die Datei radwegweiser.gpkg aus, die Sie zusammen mit der QGIS-Projektdatei in ein eigenes Projektverzeichnis kopiert haben.

    .. image:: img/new_db_connection2.png
        :class: with-border float-right

Mit der Bestätigung über *Öffnen* haben sie die Datenbankverbindung angelegt und können das Plugin verwenden. 

.. hint:: 
    Das Plugin verwendet die Pfadinformationen des so konfigurierten GeoPackage,
    um eine SpatiaLite-Verbindung herzustellen. Das GeoPackage muss **radwegweiser.gpkg** heißen.

**Um die neue Datenbankverbindung zu verwenden, beenden Sie QGIS und starten Sie es neu.**

|np|

Im Anschluss wird Ihnen das mit Informationen gefüllte Bedienfeld angezeigt. 

    .. image:: img/bedienfeld_radwegweiser.png
        :class: with-border float-right

