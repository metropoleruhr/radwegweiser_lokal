# -*- coding: utf-8 -*-

from builtins import range
from qgis.PyQt.QtCore import Qt
from qgis.PyQt import QtSql
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtSql import QSqlDatabase

from qgis.core import Qgis, QgsMessageLog

import json
#import resources

ID, GEOM, VPINFO_ID, RVN_ID, PFOSTENNR, BEMERKUNG, MONTAGE, KNOTEN_ID, WEGWEISERTYP_ID, PFOSTENTYP_ID, MATERIAL_ID, EIGENTUEMER_ID, STATUS_ID, \
    KNOTEN, KNOTENPUNKTHUT, GEMEINDE = list(range(16))


class PfostenTableModel(QtSql.QSqlRelationalTableModel):

    def __init__(self, parent=None, db=QSqlDatabase()):
        super(PfostenTableModel, self).__init__(parent, db)
        self.LockedColumns = [ID]

    def flags(self, index):
        flags = QtSql.QSqlTableModel.flags(self, index)
        if index.column() not in self.LockedColumns:
            flags |= Qt.ItemIsEditable
        else:
            flags = Qt.ItemIsEnabled | Qt.ItemIsSelectable
        return flags

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return None

        if role == Qt.TextAlignmentRole:
            if index.column() in (ID, PFOSTENNR):
                return Qt.AlignCenter | Qt.AlignVCenter
            else:
                return Qt.AlignLeft | Qt.AlignVCenter

        if index.column() == ID:
            if role == Qt.ToolTipRole:
                quelle = self.record(index.row()).value("source")
                return quelle

        if index.column() == STATUS_ID:
            if role == Qt.DecorationRole:	# Zugriff über column index statt name, da name des relationalen Feldes unbekannt
                status = self.record(index.row()).value(STATUS_ID)
                # QgsMessageLog.logMessage('PfostenTableModel status %s' % status, 'Radwegweiser lokal', Qgis.Info)
                if status == 'unbekannt':
                    return QIcon(":/plugins/radwegweiser/icons/star_red.png")
                elif status == 'geprüft':
                    return QIcon(":/plugins/radwegweiser/icons/star_green.png")
                elif status == 'in Prüfung':
                    return QIcon(":/plugins/radwegweiser/icons/star_yellow.png")
                elif status == 'Ortstermin':
                    return QIcon(":/plugins/radwegweiser/icons/bewirtschafter.png")
                else:
                    return QIcon(":/plugins/radwegweiser/icons/star_red.png")

            elif role == Qt.ToolTipRole:
                return "Status: %s" % str(self.record(index.row()).value(STATUS_ID))
            elif role == Qt.DisplayRole:
                return self.record(index.row()).value(STATUS_ID)
                # return ''

        if QtSql.QSqlTableModel.data(self, index, role) == None:
            return None
        else:
            return QtSql.QSqlTableModel.data(self, index, role)

    def setData(self, index, value, role=Qt.EditRole):
        if value == '' or value == None:
            QtSql.QSqlTableModel.setData(self, index, None, role)
        else:
            QtSql.QSqlTableModel.setData(self, index, value, role)

        self.dirty = True
        # self.dataChanged.emit(index, index)
        return True
