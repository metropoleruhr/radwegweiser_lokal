.. _layout_uebersicht:

QGIS-Layout
################################################

QGIS ermöglicht es, in einem **Drucklayout** Seiten mit Karteninhalten und weiteren dynamischen Elementen 
zu konfigurieren. Mit der **Atlas-Funktion** können dann für mehrere Objekte (hier: Pfosten an einem Knoten) 
Druckseiten als mehrseitiges PDF gespeichert werden.

Hierbei werden für jeden Pfosten des Knotens:

* die **Übersichts-, Detail- und Straßenkarte** auf den Pfosten zentriert
  und der aktuelle Wegweiser rot hervorgehoben
* die **Wegweisergrafiken** des Pfostens (aus dem lokalen Dateisystem) geladen
  und zusammen mit ihrer ID und Richtungsangabe in einem HTML-Rahmen ausgegeben 
* **Fotodateien** (aus dem lokalen Dateisystem) geladen und nebst Titel auf der zweiten und ggf. weiteren Seiten
  des Drucklayouts ausgegeben 
* weitere Textelemente (Gemeinde, Koordinaten usw.) mit Inhalt gefüllt
* die Seiten zur PDF-Datei hinzugefügt.


.. figure:: img/printlayout1.png
    :figclass: align-center 
    :width: 90%

    QGIS-Drucklayout "radwegweiser" mit ausgewähltem HTML-Rahmen (Schilder)


.. hint:: Die genauen Einstellungen kann man sich am besten selbst durch Öffnen dieses Layouts 
          in QGIS erschließen. Es ist in der QGIS-Projektvorlage :ref:`QGIS-Projektvorlage <qgisprojekt_install>`
          enthalten.

Als sog. **Abdeckungslayer** für den Atlas wird der **Layer Pfosten (Auswahl)** verwendet. Das bedeutet,
der Atlas nutzt diesen Layer, um für jeden Pfosten (des Knotens) die Inhalte im Drucklayout zu aktualisieren.
Der Layer nutzt die Daten einer komplexen Datenbanksicht, die HTML-Quellcode zu Wegweisern und Fotos ausgibt.   

Der Abdeckungslayer (Pfosten) wird selbst nicht in den Karten dargestellt.
Die Karten visualisieren stattdessen die Wegweiser an den Pfosten, die in eine bestimmte Richtung zeigen
(Pfeilwegweiser) bzw. aus einer bestimmten Richtung sichtbar sind (Zwischen-/Tabellenwegweiser).

In QGIS kann mit sog. **Kartenthemen** eine bestimmte Sichtbarkeit und Visualisierung von Layern
unter einer Bezeichnung abgespeichert werden. Im Drucklayout nutzt die Übersichtskarte (1:50.000)
das Thema "uebersichtskarte" und die Detailkarte (1:500) das Thema "detailkarte". |br| 
Änderungen an diesen Kartenelementen im Drucklayout müssen daher im jeweiligen Kartenthema erfolgen. 

.. warning:: Die Straßenkarte (1:2.000) ist nicht auf ein Kartenthema eingestellt.
          Sie stellt die Layer so dar, wie sie zum Zeitpunkt der PDF-Erstellung sichtbar und gestylt sind.
          **Sie können vor der PDF-Erstellung zum Kartenthema 'arbeitskarte' umschalten, um die gewohnte
          Layerdarstellung im PDF zu erhalten.** 



