.. _truetype:

Schriftart "HBR NRW eng" installieren
*****************************************************

Im  Unterverzeichnis python\\plugins\\radwegweiser_lokal des QGIS-Profilverzeichnisses findet sich die
TrueType-Schriftdatei hbr_nrw1_v[VERSIONSNUMMER].ttf. Es handelt sich um die Schrift, die für die Schildergrafik
verwendet wird und die dort verwendete Symbole wie Bahnhöfe, Radstationen usw. visualisiert.

  .. figure:: img/hbr_nrw1.png
      :figclass: align-center
      :width: 300pt

Wählen Sie im Plugin-Verzeichnis die Datei z.B. hbr_nrw1_v12.ttf aus und wählen im Kontextmenü 
(rechte Maustaste) den Eintrag "Für alle Benutzer installieren".

  .. figure:: img/truetype_install.png
      :figclass: align-center
       
      Schriftart installieren

.. hint:: Die Schriftart wird von QGIS erst nach einem Neustart erkannt.

|np|
