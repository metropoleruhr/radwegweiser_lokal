
import sys


from PyQt5 import QtSql
from PyQt5.QtCore import Qt, QObject, QSettings, QFileInfo, QThread, QSortFilterProxyModel, QCoreApplication
from PyQt5.QtWidgets import QApplication, QComboBox, QCompleter, QHeaderView, QAction, QDataWidgetMapper, \
    QFileDialog, QPushButton, QStyleOptionButton, QStyle, QStyledItemDelegate, QMessageBox, QShortcut 
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QColor, QIcon, QKeySequence

class AutoCompleteComboBox(QComboBox):
    def __init__(self,  parent=None):
        super(AutoCompleteComboBox, self).__init__(parent)

        self.setFocusPolicy(Qt.StrongFocus)
        self.setEditable(True)
        self.completer = QCompleter(self)

        # always show all completions
        self.completer.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
        self.pFilterModel = QSortFilterProxyModel(self)
        self.pFilterModel.setFilterCaseSensitivity(Qt.CaseInsensitive)

        self.completer.setPopup(self.view())

        self.setCompleter(self.completer)

        self.lineEdit().textEdited[unicode].connect(self.pFilterModel.setFilterFixedString)
        self.completer.activated.connect(self.setTextIfCompleterIsClicked)

    def setModel(self, model):
        super(AutoCompleteComboBox, self).setModel(model)
        self.pFilterModel.setSourceModel(model)
        self.completer.setModel(self.pFilterModel)

    def setModelColumn(self, column):
        self.completer.setCompletionColumn(column)
        self.pFilterModel.setFilterKeyColumn(column)
        super(AutoCompleteComboBox, self).setModelColumn(column)


    def view(self):
        return self.completer.popup()

    def index(self):
        return self.currentIndex()

    def setTextIfCompleterIsClicked(self, text):
        if text:
            index = self.findText(text)
            self.setCurrentIndex(index)
