# -*- coding: utf-8 -*-

from builtins import range
from qgis.PyQt.QtCore import Qt, QSortFilterProxyModel
from qgis.PyQt import QtSql
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtSql import QSqlDatabase

from qgis.core import Qgis, QgsMessageLog

ID, SCHILD_NR, ZEILE, ZIEL, NAHZIEL, ENGSCHRIFT, SCHRIFTHOEHE, HINWEIS, ENTFERNUNG, WEGWEISER_ID, PFOSTEN, KNOTEN, WEGWEISER, \
ZIELPIKTOGRAMM1_ID, ZIELPIKTOGRAMM2_ID, STRECKENPIKTOGRAMM1_ID, STRECKENPIKTOGRAMM2_ID = list(range(17))


class WegweiserInhaltSortFilterModel(QSortFilterProxyModel):
    def __init__(self, parent=None):
        super(WegweiserInhaltSortFilterModel, self).__init__(parent)

    """Nach Schild und Zeile aufsteigend sortieren, damit Wegweiser-Grafik korrekt"""
    def lessThan(self, left, right):
        try:
            leftSchild = self.sourceModel().data(self.sourceModel().index(left.row(), SCHILD_NR)) + 1
            rightSchild = self.sourceModel().data(self.sourceModel().index(right.row(), SCHILD_NR)) + 1
            leftZeile = self.sourceModel().data(self.sourceModel().index(left.row(), ZEILE))
            rightZeile = self.sourceModel().data(self.sourceModel().index(right.row(), ZEILE))
            # QgsMessageLog.logMessage('lessTahn %s %s %s %s' % (leftSchild, leftZeile, rightSchild, rightZeile), 'Radwegweiser lokal', Qgis.Info)

            return (leftSchild + leftZeile) < (rightSchild + rightZeile)
        except Exception as e:
            QgsMessageLog.logMessage('Fehler in lessThan: %s' % e, 'Radwegweiser lokal', Qgis.Critical)  

