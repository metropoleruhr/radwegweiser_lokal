# -*- coding: utf-8 -*-

from builtins import range
from qgis.PyQt.QtCore import Qt
from qgis.PyQt import QtSql
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtSql import QSqlDatabase

import json
#import resources

ID, USERNAME, FULLNAME, EMAIL, TELEFON, MOBIL, KOMMENTAR, PROFILVERZEICHNIS, TEAM_ID = list(range(9))


class PersonTableModel(QtSql.QSqlTableModel):

    def __init__(self, parent=None, db=QSqlDatabase()):
        super(PersonTableModel, self).__init__(parent, db)
        self.LockedColumns = [ID]

    def flags(self, index):
        flags = QtSql.QSqlTableModel.flags(self, index)
        if index.column() not in self.LockedColumns:
            flags |= Qt.ItemIsEditable
        else:
            flags = Qt.ItemIsEnabled | Qt.ItemIsSelectable
        return flags

    def data(self, index, role=Qt.DisplayRole):
        if role == Qt.TextAlignmentRole:
            return Qt.AlignLeft | Qt.AlignVCenter

        if QtSql.QSqlTableModel.data(self, index, role) == None:
            return None
        else:
            return QtSql.QSqlTableModel.data(self, index, role)
