.. _python:

Zu installierende python-Bibliotheken
*****************************************************

Das Plugin nutzt diverse python-Module für weitergehende Funktionen.

Für das Freistellen der Schildergrafik wird `Pillow <https://pypi.org/project/Pillow/>`_ (ein Fork der Python Imaging Library PIL) verwendet. 
Installieren Sie es durch Ausführen des folgenden Befehls auf der Kommandozeile (OSGeo4W-Shell): 

.. code-block::

  pip install Pillow



