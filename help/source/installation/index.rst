.. _installation:

Installation
##############

Die Nutzung der **Erweiterung Radwegweiser** erfordert einige Vorbereitungen, die im Folgenden erläutert werden.
 

.. toctree::
   :maxdepth: 1

   python.rst
   plugin.rst
   truetype.rst
   qgisprojekt.rst
   connection.rst



