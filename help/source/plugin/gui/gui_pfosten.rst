.. _gui_pfosten:

Reiter Pfosten
********************

Der **Reiter Pfosten** listet die :ref:`Pfosten <begriff_pfosten>` auf und ermöglicht die Bearbeitung ihrer Eigenschaften
sowie der am Pfosten angebrachten Wegweiser und deren Inhalte.
Im Regelfall werden die Pfosten, die im aktuell sichtbaren Kartenausschnitt liegen aufgelistet.
Durch Filtern können aber auch Pfosten mit bestimmten Eigenschaften gefunden und aufgelistet werden.

Die Tabelle der Pfosten enthält die Spalten (eindeutige) **ID**, **Pfosten-Nummer**, zugehöriger Knoten (ID) 
und **Status** des Pfostens (Icon).

Das Bedienelement **Schnellfilter** oberhalb der Tabelle erlaubt die Eingabe einer Zeichenkette und zeigt die Pfosten an,
deren ID oder Pfosten-Nr. diese Zeichenkette enthalten. Es müssen mindestens 2 Zeichen eingegeben sein, 
damit die Filterung aktiviert wird. Mit der Schaltfläche |I1| heben Sie die Filterung komplett auf.

Oberhalb der Tabelle der Pfosten befinden sich die folgende **Bedienelemente**:

* **Auswahlfeld Gruppe** ermöglicht die Filterung aller Pfosten einer ausgewählten Gruppe
* |I6| **filtert** die Liste auf die Pfosten im aktuellen Kartenausschnitt 
* |I2| **zoomt** den Kartenausschnitt auf die in der Tabelle ausgewählten Pfosten (mit 30m Puffer)
* Kontrollkästchen **Layer filtern** wenn angehakt: filtert den Layers "Pfosten (Auswahl)" 
  auf den jeweils aktuell ausgewählten Pfosten 
* |I7| **PDF-Katasterblatt** für alle Pfosten des Knotens oder |br| (Strg-Klick) den aktuellen Pfosten erstellen 
* |I3| **löscht** den ausgewählten Pfosten nach Bestätigung einer Sicherheitsabfrage |br| 
  (wenn dem Pfosten keine Wegweiser mehr zugewiesen sind)
* |I4| Änderungen an Eigenschaften des Pfostens **speichern**. 
  Wird aktiv, wenn z.B. die Pfosten-Nr. geändert wurde. 
  Änderungen an Pfostentyp, Status, Material und Eigentümer werden unmittelbar gespeichert. 
* |I5| Änderungen an Eigenschaften des Pfostens **verwerfen** und Formular neu füllen.

.. figure:: img/reiter_pfosten2.png
       :figclass: align-center
       :scale: 75%
       



.. |I1| image:: img/filter_aufheben.png
.. |I2| image:: img/zoom_route.png
.. |I3| image:: img/delete.png
.. |I4| image:: img/save.png
.. |I5| image:: img/refresh.png
.. |I6| image:: img/mActionZoomToSelected.png
.. |I7| image:: img/pdficon_small.png
.. |I8| image:: img/delete.png
.. |I9| image:: img/save.png
.. |I10| image:: img/refresh.png
.. |I11| image:: img/add_layer.png


Pfostendetails
===============

Im Unterreiter Pfostendetails lassen sich die Eigenschaften eines Pfostens ändern.

* Die Pfosten-Nr. eingeben 
* Dem Pfosten eine Knoten-ID zuordnen durch direkte Eingabe oder mittels |I11| den nächstgelegenen Knoten (in max. 150m Entfernung)
* Festlegen, welcher Knotenpunkthut am Pfosten zu montieren ist
  (0 = kein Knotenpunkthut)
* Pfostentyp, Status, Material und Eigentümer auswählen
* eine Bemerkung und ggf. Hinweise zur Montage ergänzen

Wer darf Pfostendaten ändern?
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Einem Pfosten können eine oder mehrere Gruppen zugeordnet sowie
festgelegt werden, ob diese Gruppe Pfostendaten ändern darf. 

.. hint:: Die Funktion der Bearbeitungsrechte ist noch zu implementieren.
