# -*- coding: utf-8 -*-

from builtins import range
from qgis.PyQt.QtCore import Qt
from qgis.PyQt import QtSql
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtSql import QSqlDatabase

import json
#import resources

WEGWEISER_ID, EINSCHUB_ID, BEZEICHNUNG, TYP, POSITION_NR, LOGO_URL = list(range(6))


class WegweiserEinschubTableModel(QtSql.QSqlRelationalTableModel):

    def __init__(self, parent=None, db=QSqlDatabase()):
        super(WegweiserEinschubTableModel, self).__init__(parent, db)
        self.LockedColumns = []

    def flags(self, index):
        flags = QtSql.QSqlTableModel.flags(self, index)
        if index.column() not in self.LockedColumns:
            flags |= Qt.ItemIsEditable
        else:
            flags = Qt.ItemIsEnabled | Qt.ItemIsSelectable
        return flags

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return None

        if role == Qt.TextAlignmentRole:
            if index.column() in (WEGWEISER_ID, EINSCHUB_ID, POSITION_NR):
                return Qt.AlignCenter | Qt.AlignVCenter
            else:
                return Qt.AlignLeft | Qt.AlignVCenter

        if QtSql.QSqlTableModel.data(self, index, role) == None:
            return None
        else:
            return QtSql.QSqlTableModel.data(self, index, role)

    def setData(self, index, value, role=Qt.EditRole):
        if value == '' or value == None:
            QtSql.QSqlTableModel.setData(self, index, None, role)
        else:
            QtSql.QSqlTableModel.setData(self, index, value, role)

        self.dirty = True
        # self.dataChanged.emit(index, index)
        return True
