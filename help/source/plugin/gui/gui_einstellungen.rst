.. _gui_einstellungen:

Reiter Einstellungen
************************

Der **Reiter Einstellungen** ermöglicht die Konfiguration verschiedener Verzeichnispfade.
Dies sind:

* Das Verzeichnis für Grafikdateien, in dem Wegweisergrafiken erstellt werden
* Das Ausgabeverzeichnis für PDF-Katasterblätter
* Das Verzeichnis für Grafikdateien, die als Einschübe zugeordnet werden können
* Das Verzeichnis, in dem der Dateiauswahldialog für Fotodateien geöffnet werden soll 
* Das Verzeichnis, in das heruntergeladene Fotodateien für einen Pfosten (\*.zip) gespeichert werden.

  .. figure:: img/reiter_einstellungen.png
       :figclass: align-center
       :scale: 90%

Darüber hinaus kann mit dem **Kontrollkästchen automatische Fotoanzeige** festgelegt werden, ob
die einem Pfosten zugeordneten Fotodateien bei jeder Pfostenauswahl geladen und angezeigt werden sollen.
Dies führt zu einer gewissen Verzögerung beim Wechsel zwischen Pfosten.

Um das Plugin **Radwegweisung in QGIS** auch in anderen Bundesländern nutzen zu können,
kann die **Schilderfarbe** hier eingestellt werden. In Reinland-Pfalz wird die Beschilderung z.B.
in **verkehrsgrün** ausgegeben.

