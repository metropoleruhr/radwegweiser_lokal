.. _gui_wegweiser:

Unterreiter Wegweiser
**********************

Der **Unterreiter Wegweiser** listet zum ausgewählten Pfosten die Wegweiser auf 
und ermöglicht die Bearbeitung ihrer Eigenschaften.

Die Tabelle der Wegweiser enthält die Spalten (eindeutige) **ID**, **Richtung** und **Drehung**, 
**Wegweisertyp**, **Pfeil** und **Abfolge**. 

Oberhalb der Tabelle der Wegweiser befinden sich die folgende **Schaltflächen**:

* |I1| **Schild zeichnen** aktualisiert aus den Wegweiserdaten und Schildinhalten 
  die Grafik im Reiter **Wegweiser-Grafik**
* |I2| **speichern** speichert die Wegweisergrafik lokal im Dateisystem.
* |I3| fügt dem Pfosten einen Wegweiser hinzu  
* |I4| **löscht** den ausgewählten Wegweiser nach Bestätigung einer Sicherheitsabfrage |br| 
  (wenn dem Wegweiser keine Schildinhalte mehr zugewiesen sind)
* |I5| Änderungen an Eigenschaften des Wegweisers **speichern**. 
  Wird aktiv, wenn z.B. die Drehung oder Abfolge geändert wurde. 
  Änderungen an Richtung, Wegweisertyp und Pfeilart werden unmittelbar gespeichert. 
* |I6| Änderungen an Eigenschaften des Wegweisers **verwerfen** und Formular neu füllen.

|np| 

Der Reiter Wegweiser enthält unterhalb der Tabelle weitere **Reiter**

* zur Anzeige und Eingabe von **Wegweiser-Details**
* zur Verwaltung der anzuzeigenden **Schildinhalte** des Wegweisers
* zur Zuordnung von **Einschüben** zu Themenrouten und Knotenpunkten
* sowie zur Anzeige der aus diesen Daten gerenderten **Wegweiser-Grafik** 

.. figure:: img/reiter_wegweiser1.png
       :figclass: align-center
       :scale: 80%
       
       Unterreiter *Wegweiser*

====================
Wegweiser-Details
====================

Hier gilt es, zuerst den Wegweisertyp und die Pfeilart festzulegen. 

.. figure:: img/reiter_wegweiser_wegweisertyp.png
    :figclass: align-left 
    :width: 300px

Der :ref:`Wegweisertyp <wegweisertyp>` entscheidet im Besonderen über die graphische Gestaltung des Wegweisers.
Wählen Sie hier zwischen Pfeil-, Tabellen- und Zwischenwegweiser. (Die anderen Typen werden noch nicht unterstützt.) 

.. figure:: img/reiter_wegweiser_pfeilart.png
    :figclass: align-left 
    :width: 200px

Die **Pfeilart** bestimmt den auf dem Wegweiser darzustellenden Pfeil. 
Links- oder geradeaus-orientierte Pfeile werden links vom Schildinhalt positioniert,
rechts-orientierte rechts davon. Entsprechend beeinflusst dies die Pfeilrichtung der Pfeilwegweiser. 

.. figure:: img/reiter_wegweiser_richtung.png
    :figclass: align-right 
    :width: 200px

Die **Richtung** des Wegweisers bestimmt bei Tabellen- und Zwischenwegweisern die Richtung 
**aus der diese sichtbar** sind. Bei Pfeilwegweisern bestimmt sie hingegen **in welche Richtung sie weisen**.
Es kann eine zusätzliche Drehung (Gradangabe) eingegeben werden, um die reale Ausrichtung der Wegweiser abzubilden. 

Im Feld **Kommentar** kann ein textlicher Hinweis zu diesem Wegweiser eingegeben werden.
Die einzugebende "historische ID" und das Feld "weist auf" haben derzeit keine Bedeutung.

.. figure:: img/reiter_wegweiser1a.png
    :figclass: align-center 
    :width: 90%

.. hint:: Wurde der Wegweiser fertig bearbeitet und die Wegweisergrafik als Datei lokal gespeichert,
          wird unterhalb des Eingabeformulars die zuletzt gespeicherte Grafikdatei angezeigt. 


====================
Schildinhalte
====================

Der Reiter **Schildinhalte** zeigt die dem Wegweiser zugordneten Inhalte tabellarisch an.
Dies sind die anzuzeigenden Ortsangaben (**Hinweis**) nebst **Entfernung**, z.B. Bhf Kupferdreh 0,5 km.

Wo sie in der Grafik positioniert werden, ergibt sich aus der Angabe zu **Schild** und **Zeile** auf dem Schild. 
Die laufende Nummer wird aus der Anzahl der Schildinhalte berechnet. 
Sie kann mittels **Taste F2** direkt in der Tabelle editiert werden.

.. figure:: img/reiter_wegweiser2.png
    :figclass: align-center 
    :width: 80%

|np|

Es können bis zu zwei :ref:`Ziel- und Streckenpiktogramme <begriff_zielpiktogramm>` je Zeile hinzugefügt werden.
Zielpiktogramme werden links vom Ortshinweis positioniert, Streckenpiktogramme rechts davon.

.. figure:: img/zielpiktogramme.png
    :figclass: align-right 
    :width: 75%
.. figure:: img/streckenpiktogramme.png
    :figclass: align-right 
    :width: 80%

Wird das Streckenpiktogramm **Steigung** bzw. **Gefälle** ausgewählt, so muss die Prozentangabe
zur Steigung bzw. zum Gefälle numerisch eingegeben werden. 
(Steigungen/Gefälle sollten erst ab 4% vermerkt werden.)

Oberhalb der Tabelle der Schildinhalte befinden sich folgende **Schaltflächen**:

* |I3| **Neuen Schildinhalt** für den Wegweiser erstellen
* |I4| den ausgewählten Inhalt nach Bestätigung einer Sicherheitsabfrage **löschen** 
* |I5| Änderungen am Schildinhalt **speichern**. 
  Wird aktiv, wenn z.B. der Ortshinweis oder die Entfernung geändert wurde und das Bedienelement 
  seinen Focus verloren hat (z.B. durch die Tab-Taste). 
  Änderungen an Ziel- bzw. Streckenpiktogrammen werden unmittelbar gespeichert. 


====================
Einschübe
====================

Der Reiter **Einschübe** zeigt die dem Wegweiser zugordneten :ref:`Einschübe  <begriff_einschub>` tabellarisch an.
Dies sind graphische Hinweise auf Themenrouten (z.B. den Ruhrtalradweg) und Knotenpunkte.
Angezeigt werden die eindeutige ID, die Bezeichnung des Einschubs und die Position am Wegweiser.

.. figure:: img/reiter_wegweiser3.png
    :figclass: align-center 
    :width: 60%


Oberhalb der Tabelle der Einschübe befinden sich folgende **Bedienelemente**:

* **Kombinationsfeld Einschub** zur Auswahl einer Themenroute bzw. eines Knotenpunktes.
  Das Feld unterstützt Auto-Vervollständigung zur komfortablen Suche eines Einschubes durch Eingabe einer Zeichenkette.
* |I3| **Hinzufügen** des ausgewählten Einschubs zum Wegweiser
* |I4| **Löschen** des ausgewählten Einschubs nach Bestätigung einer Sicherheitsabfrage
* |I5| Änderungen am Einschub (Position) **speichern**. 
  Wird aktiv, wenn die Position geändert wurde. 

.. hint:: Nach Hinzufügen bzw. Auswählen eines Einschubes wird die Einschubgrafik 
          entsprechend ihrer Positionierung unterhalb der Tabelle angezeigt.  



====================
Wegweiser-Grafik
====================

Aus den Eigenschaften des Wegweisers, den Schildinhalten und Einschüben wird eine 
graphische Darstellung des Wegweisers erzeugt und im Reiter **Wegweiser-Grafik** angezeigt.

.. figure:: img/reiter_wegweiser4.png
    :figclass: align-left 
    :width: 300px

Die hier angezeigte Grafik dient zur Überprüfung und wird als Rasterdatei im Dateisystem gespeichert,
wenn die Schaltfläche |I2| **speichern** betätigt wird.






.. |I1| image:: img/stylemanager.png
.. |I2| image:: img/owncloud.png
.. |I3| image:: img/add.png
.. |I4| image:: img/delete.png
.. |I5| image:: img/save.png
.. |I6| image:: img/refresh.png
.. |I7| image:: img/filter_aufheben.png
.. |I8| image:: img/zoom_route.png

