# -*- coding: utf-8 -*-

from builtins import range
from qgis.PyQt.QtCore import Qt
from qgis.PyQt import QtSql
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtSql import QSqlDatabase

import json
#import resources

ID, GRUPPE, BEZEICHNUNG, SCHREIBEN  = list(range(4))


class KnotenGruppenTableModel(QtSql.QSqlRelationalTableModel):

    def __init__(self, parent=None, db=QSqlDatabase()):
        super(KnotenGruppenTableModel, self).__init__(parent, db)
        self.LockedColumns = [ID, GRUPPE, BEZEICHNUNG, SCHREIBEN]

    def flags(self, index):
        flags = QtSql.QSqlTableModel.flags(self, index)
        if index.column() not in self.LockedColumns:
            flags |= Qt.ItemIsEditable
        else:
            flags = Qt.ItemIsEnabled | Qt.ItemIsSelectable
        return flags

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return None

        if role == Qt.TextAlignmentRole:
            if index.column() in (ID, GRUPPE, SCHREIBEN):
                return Qt.AlignCenter | Qt.AlignVCenter
            else:
                return Qt.AlignLeft | Qt.AlignVCenter

        # if index.column() == ID:
        #     if role == Qt.ToolTipRole:
        #         quelle = self.record(index.row()).value("source")
        #         return quelle

        if index.column() == SCHREIBEN:
            if role == Qt.DisplayRole:
                return ''
            elif role == Qt.DecorationRole:
                schreiben = self.record(index.row()).value("schreiben")
                if schreiben:
                    icon = QIcon(":/plugins/radwegweiser/icons/star_green.png")                    
                else:
                    icon = QIcon(":/plugins/radwegweiser/icons/star_red.png")                    
                return icon
            elif role == Qt.ToolTipRole:
                schreiben = self.record(index.row()).value("schreiben")
                if schreiben:
                    text = "darf Pfosten edtieren"
                else:
                    text = "darf Pfosten sehen"
                return text

        if QtSql.QSqlTableModel.data(self, index, role) == None:
            return None
        else:
            return QtSql.QSqlTableModel.data(self, index, role)

    def setData(self, index, value, role=Qt.EditRole):
        if value == '' or value == None:
            QtSql.QSqlTableModel.setData(self, index, None, role)
        else:
            QtSql.QSqlTableModel.setData(self, index, value, role)

        self.dirty = True
        # self.dataChanged.emit(index, index)
        return True
