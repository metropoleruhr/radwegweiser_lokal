.. _systemskizze_uebersicht:

Zusammenfassung
################################################

In nachfolgender Abbildung sind die einzelnen Komponenten und ihr Zusammenspiel graphisch dargestellt:

.. figure:: img/prinzipskizze.png
    :figclass: align-center 
    :width: 100%

    Systemskizze Radwegweisungskataster

* **QGIS** stellt u.a. Pfosten und Wegweiser aus der Geodatenbank (GeoPackage) als Kartenlayer dar.
* Das **Plugin Radwegweiser** dient der Darstellung und Bearbeitung der Wegweisungsdaten 
  aus der Geodatenbank (SpatiaLite).
* Vom Plugin gerenderte Wegweisergrafiken und ausgewählte Fotos werden im Dateiverzeichnis abgelegt. 
* Grafikdateien/Fotos werden über das Dateisystem im Plugin angezeigt und im Layout geladen.
* Ein **QGIS-Layout** erzeugt einen sog. Atlas für alle Pfosten eines Knotens.
  Eine komplexe Datenbanksicht stellt den HTML-Code zur Einbindung der Wegweisergrafiken und Fotos bereit.
* Der in das Dateisystem exportierte PDF-Atlas enthält alle Katasterblätter zur Wegweisung an einem Knoten. 

Zukünftig können so die Wegweisungsdaten als Kartendienst (WMS/WFS) bereitgestellt und die PDF-Katasterblätter
mit anderen Systemen beim Land NRW ausgetauscht werden. 

