.. _plugin_gui:

#####################################
Bedienhinweise
#####################################

Nachfolgend werden die einzelnen Bedienoberflächen und -elemente dokumentiert.
Im Anschluss werden typische Vorgehensweisen zur Bearbeitung von Pfosten und Wegweisern erläutert.

.. toctree::
   :maxdepth: 2

   gui/index.rst
   vorgehensweise/index.rst



   

