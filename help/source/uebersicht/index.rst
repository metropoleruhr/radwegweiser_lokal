.. _uebersicht:

Übersicht
#################################

Im folgenden werden für ein besseres Verständnis der Aufbau und die Funktionsweise 
wesentlicher Komponenten beschrieben. |br|

.. figure:: img/pdf_seiten1.png
    :figclass: align-center
    :scale: 60%
    
    *PDF-Katasterblatt zu Pfosten 4552*

|np|

.. hint:: Die für die konkrete Bedienung erforderlichen
         :ref:`Bedienoberflächen und Vorgehensweisen <plugin_gui>` werden im Anschluss beschrieben.
         **Nur an der Verwendung des Plugins Interessierte können die folgenden Inhalte überspringen.**

Initiale Anforderung an die QGIS-Erweiterung war, die Erstellung eines sog. PDF-Katasterblattes
zur Dokumentation der Beschilderung an einem Knoten.

Das PDF-Katasterblatt dokumentiert für alle Pfosten des Knotens

* die Schildergrafiken der verschiedenen Wegweiser
* ggf. zusätzliche Routen- oder Knotenpunkteinschübe (hier: KP 54)
* ggf. Knotenpunkthut des Pfostens (hier: 83)
* den Standort des Pfostens in einer Übersichtskarte (Stadtteil), 
  einer Detailkarte (Luftbild) und einer Straßenkarte
* Fotos, die den Standort weiter dokumentieren
* Gemeinde, geographische Position usw. 

Die Erstellung und Bereitstellung des PDF-Katasterblattes erfordert folgende Komponenten bzw. Schritte:

* Ein :ref:`GeoPackage <datenbank_uebersicht>` zur Speicherung der erforderlichen (Geo)Daten 
* Eingabeoberflächen, um Pfosten, Wegweiser und Schildinhalte einzugeben und Fotos zuzuordnen
* Je Wegweiser wird aus den eingegebenen Schilderdaten mittels QML-Code und QtQuick eine 
  :ref:`Wegweisergrafik <wegweisergrafik_uebersicht>` berechnet
* Wegweisergrafiken werden als PNG-Datei im lokalen Dateisystem gespeichert
* ein :ref:`QGIS-Layout <layout_uebersicht>` positioniert die Schildergrafiken am Pfosten, aktualisiert 
  die Übersichts-, Detail- und Straßenkarte, lädt zugeordnete Fotos und speichert die Katasterblätter 
  des Knotens als PDF

.. toctree::
   :maxdepth: 2

   datenbank.rst
   wegweisergrafik.rst
   layout.rst
   qgisprojekt.rst
   systemskizze.rst


