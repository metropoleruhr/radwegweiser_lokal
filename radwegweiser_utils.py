# -*- coding: utf-8 -*-

from __future__ import print_function
from ast import And
from builtins import str
from builtins import zip
from builtins import range
from qgis.PyQt.QtCore import Qt, QObject, pyqtSignal, QSettings, QVariant, QRegExp, QDate
from qgis.PyQt.QtGui import QStandardItemModel, QStandardItem, QIcon, QPixmap, QRegExpValidator

from PyQt5.QtWidgets import QApplication, QFileDialog, QMessageBox, QComboBox, QHBoxLayout, QLabel, QLineEdit, QDateEdit, QCheckBox, QPushButton, QToolButton, QSpacerItem, QSizePolicy

from qgis.core import Qgis, QgsProject, QgsMapLayerType, QgsMessageLog, QgsProject, QgsVectorLayer, QgsRasterLayer, QgsGeometry, QgsPointXY, QgsPoint, QgsRectangle, QgsDataSourceUri, \
    QgsVectorFileWriter, QgsSymbol, QgsRuleBasedRenderer,  QgsFeature, QgsFeatureRequest, QgsSpatialIndex, QgsCoordinateReferenceSystem, \
    QgsPointLocator, QgsRenderContext, QgsWkbTypes, QgsApplication, QgsAuthManager, QgsAuthMethodConfig, QgsProviderRegistry
from qgis.gui import QgsVertexMarker, QgsRubberBand, QgsMessageBar, QgsMapToolEmitPoint

from PyQt5.QtSql import QSqlTableModel, QSqlQuery

from pathlib import Path
import re
import os

import sys
import owncloud
import sqlite3
import spatialite
from osgeo import ogr


def addVectorLayer(self, layersource, layername, protocol):
    try:
        theLayer = QgsVectorLayer(layersource, layername, protocol)
        QgsProject.instance().addMapLayer(theLayer, False)
        root = QgsProject.instance().layerTreeRoot()
        layerTreeLayer = root.insertLayer(0, theLayer)
        # theLayer.loadNamedStyle('P:/qgis/styles/topologie_punkte.qml')
        QgsMessageLog.logMessage("Layer wurde hinzugefügt: %s" % layername, 'Radwegweiser lokal', Qgis.Info)
        return layerTreeLayer.layer()
    except Exception as e:
        QgsMessageLog.logMessage('Fehler in addVectorLayer: %s' % e, 'Radwegweiser lokal', Qgis.Critical) 


def addRasterLayer(self, layersource, layername, protocol):
    try:
        theLayer = QgsRasterLayer(layersource, layername, protocol)
        QgsProject.instance().addMapLayer(theLayer, False)
        root = QgsProject.instance().layerTreeRoot()
        layerTreeLayer = root.insertLayer(0, theLayer)
        # theLayer.loadNamedStyle('P:/qgis/styles/topologie_punkte.qml')
        QgsMessageLog.logMessage("Layer wurde hinzugefügt: %s" % layername, 'Radwegweiser lokal', Qgis.Info)
        return layerTreeLayer.layer()
    except Exception as e:
        QgsMessageLog.logMessage('Fehler in addRasterLayer: %s' % e, 'Radwegweiser lokal', Qgis.Critical) 


def addFiltervalueComboBox(self, name, tabelle, beschriftung, anzahl, showColumn, rownum):
    # QgsMessageLog.logMessage("addFiltervalueComboBox: %s" % name, 'Radwegweiser lokal', Qgis.Critical)  
    try:
        # deleteItemsOfLayout(self.dockwidget.filtervalueGridLayout)

        # TableModel der Nachschlagewerte, ToDo: auslagern in einmalige Erstellung
        filterValueTableModel = QSqlTableModel(self.dockwidget, self.db) 
        filterValueTableModel.setEditStrategy(QSqlTableModel.OnManualSubmit)
        filterValueTableModel.setTable(tabelle) 
        filterValueTableModel.sort(showColumn, Qt.AscendingOrder)
        success = filterValueTableModel.select()
        if not success:
            QgsMessageLog.logMessage('Fehler addFiltervalueComboBox. Konnte Tabelle %s nicht abfragen: %s !' % (tabelle, filterValueTableModel.lastError().text()), 'Radwegweiser lokal', Qgis.Info)

        hbox = QHBoxLayout() # Layout für Label und ComboBox
        label = beschriftung
        if anzahl == 'multi':
            self.filterValues[name] = [] # leeres Array für Mehrfachauswahl-Werte
            label = beschriftung + ' (+)'
        lbl = QLabel(label)
        lbl.setFixedWidth(150)
        filterValueComboBox = QComboBox()
        filterValueComboBox.setModel(filterValueTableModel)
        filterValueComboBox.setModelColumn(showColumn)
                
        hbox.addWidget(lbl)
        hbox.addWidget(filterValueComboBox)

        self.dockwidget.filtervalueGridLayout.addLayout(hbox, rownum, 0, 1, 2)
        lblAuswahl = QLabel('')
        if anzahl == 'multi':
            self.filterValues[name+'_bezeichnung'] = []
            hboxAuswahl = QHBoxLayout() # Layout für Label mit Auswahlwerten
            hboxAuswahl.addWidget(lblAuswahl)
            # lblAuswahl.setFixedWidth(150)
            self.dockwidget.filtervalueGridLayout.addLayout(hboxAuswahl, rownum+1, 0, 1, 2)

        filterValueComboBox.activated.connect(lambda index: filterValueComboBoxActivated(self, index, filterValueTableModel, name, anzahl, showColumn, lblAuswahl, True))
    except Exception as e:
        QgsMessageLog.logMessage('addFiltervalueComboBox %s' % e, 'Radwegweiser lokal', Qgis.Info)        


def filterValueComboBoxActivated(self, index, filterValueTableModel, name, anzahl, showColumn, lblAuswahl, asis=bool):
    modelIndex = filterValueTableModel.index(index, 0)
    record = filterValueTableModel.record(modelIndex.row())
    id = record.value("fid")
    bezeichnung = record.value(showColumn)
    # QgsMessageLog.logMessage('filterValueComboBoxActivated: ID %s katalog %s' % (str(id), name) , 'Radwegweiser lokal', Qgis.Info)   

    if anzahl == 'multi':
        if asis:
            self.filterValues[name].append(id)
            # self.filterArgs[name] = '%s' % (",".join(str(x) for x in self.filterValues[name]))
            # self.filterArgs[name].append(id)
            self.filterArgs[name] = '%s' % (",".join(str(x) for x in self.filterValues[name]))
            self.filterValues[name+'_bezeichnung'].append(bezeichnung)
            lblAuswahl.setText('Auswahl: %s' % (", ".join(str(x) for x in self.filterValues[name+'_bezeichnung'])))
        else:
            self.filterValues[name].append(id)
            self.filterArgs[name] = '%s' % (",".join(str(x) for x in self.filterValues[name]))
    else:
        if asis:
            self.filterArgs[name] = id
        else:
            self.filterArgs[name] = id


def addFiltervalueLineEdit(self, name, asis, beschriftung, regexp, rownum):
    # QgsMessageLog.logMessage("addFiltervalueLineEdit: %s" % name, 'Radwegweiser lokal', Qgis.Critical)  
    try:
        self.hbox = QHBoxLayout() # Layout für Label und LineEdit
        lbl = QLabel(beschriftung)
        lbl.setFixedWidth(150)
        filterValueLineEdit = QLineEdit()
        if regexp != 'None':
            regexp = QRegExp('[%s]{1,50}' % regexp) 
            validator = QRegExpValidator(regexp) 
            filterValueLineEdit.setValidator(validator)
        filterValueLineEdit.textChanged.connect(lambda filterText: filterValueLineEditTextChanged(self, filterText, name, asis))
        self.hbox.addWidget(lbl)
        self.hbox.addWidget(filterValueLineEdit)
        # row 1 column 0 rowspan 1 columnspan 2
        self.dockwidget.filtervalueGridLayout.addLayout(self.hbox, rownum, 0, 1, 2)
        # self.setCentralWidget(widget)
    except Exception as e:
        QgsMessageLog.logMessage('addFiltervalueLineEdit %s' % e, 'Radwegweiser lokal', Qgis.Info)  

def filterValueLineEditTextChanged(self, filterText, name, asis):
    # QgsMessageLog.logMessage('filterValueLineEditTextChanged: filterText %s feld %s' % (str(filterText), name) , 'Radwegweiser lokal', Qgis.Info)   
    if asis == 'AsIs':
        self.filterArgs[name] = filterText
    if asis == 'Part':
        self.filterArgs[name] =  '%%%s%%' % filterText
    else:
        self.filterArgs[name] = filterText

def addFiltervalueDateEdit(self, name, beschriftung, rownum):
    # QgsMessageLog.logMessage("addFiltervalueDateEdit: %s" % name, 'Radwegweiser lokal', Qgis.Critical)  
    try:
        self.hbox = QHBoxLayout() # Layout für Label und LineEdit
        lbl = QLabel(beschriftung)
        lbl.setFixedWidth(150)
        filterValueDateEdit = QDateEdit()
        filterValueDateEdit.setCalendarPopup(True)
        filterValueDateEdit.setDate(QDate.currentDate())

        filterValueDateEdit.dateChanged.connect(lambda filterDate: filterValueDateEditTextChanged(self, filterDate, name, True))
        self.hbox.addWidget(lbl)
        self.hbox.addWidget(filterValueDateEdit)
        # row 1 column 0 rowspan 1 columnspan 2
        self.dockwidget.filtervalueGridLayout.addLayout(self.hbox, rownum, 0, 1, 2)
        # self.setCentralWidget(widget)
    except Exception as e:
        QgsMessageLog.logMessage('addFiltervalueDateEdit %s' % e, 'Radwegweiser lokal', Qgis.Info)  

def filterValueDateEditTextChanged(self, filterDate, name, asis=bool):
    # QgsMessageLog.logMessage('filterValueDateEditTextChanged: filterDate %s feld %s' % (filterDate.toString(Qt.ISODate), name) , 'Radwegweiser lokal', Qgis.Info)   
    self.filterArgs[name] =  "'%s'" % filterDate.toString(Qt.ISODate)


def addFiltervalueCheckBox(self, name, beschriftung, rownum):
    # QgsMessageLog.logMessage("addFiltervalueCheckBox: %s" % name, 'Radwegweiser lokal', Qgis.Critical)  
    try:
        self.hbox = QHBoxLayout() # Layout für Label und LineEdit
        lbl = QLabel(beschriftung)
        lbl.setFixedWidth(150)
        filterValueCheckBox = QCheckBox()
        # self.filterArgs[name] =  False # initial nicht gesetzt
        filterValueCheckBox.stateChanged.connect(lambda checkState: filterValueCheckBoxStateChanged(self, checkState, name, True))
        self.hbox.addWidget(lbl)
        self.hbox.addWidget(filterValueCheckBox)
        # row 1 column 0 rowspan 1 columnspan 2
        self.dockwidget.filtervalueGridLayout.addLayout(self.hbox, rownum, 0, 1, 2)
    except Exception as e:
        QgsMessageLog.logMessage('addFiltervalueDateEdit %s' % e, 'Radwegweiser lokal', Qgis.Info)  

def filterValueCheckBoxStateChanged(self, checkState, name, asis=bool):
    # QgsMessageLog.logMessage('filterValueCheckBoxStateChanged: checkState %s feld %s' % (checkState, name) , 'Radwegweiser lokal', Qgis.Info)   
    self.filterArgs[name] =  True if checkState == Qt.Checked else False


def addFiltervalueGeometry(self, name, beschriftung, rownum):
    # QgsMessageLog.logMessage("addFiltervalueGeometry: %s" % name, 'Radwegweiser lokal', Qgis.Critical)  
    try:
        self.hbox = QHBoxLayout() # Layout für Label und LineEdit
        lbl = QLabel(beschriftung)
        lbl.setFixedWidth(150)
        filterValueToolButton = QToolButton()
        icon = QIcon()
        icon.addPixmap(QPixmap(":/plugins/radwegweiser/icons/select_query.png"), QIcon.Normal, QIcon.Off)
        filterValueToolButton.setIcon(icon)
        filterValueToolButton.setCheckable(True)
        lblGeom = QLabel('')
        # lblGeom.setFixedWidth(150)

        identifyPointEmitPoint = QgsMapToolEmitPoint(self.iface.mapCanvas())
        identifyPointEmitPoint.setButton(filterValueToolButton)
        identifyPointEmitPoint.canvasClicked.connect(lambda pt: selectGeometryPoint(self, pt, name, identifyPointEmitPoint, lblGeom))
        filterValueToolButton.clicked.connect(lambda checked: selectGeometryClicked(self, identifyPointEmitPoint, checked))

        spacer = QSpacerItem(150, 10, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.hbox.addWidget(lbl)
        self.hbox.addWidget(filterValueToolButton)
        self.hbox.addItem(spacer)
        self.hbox.addWidget(lblGeom)
        # row 1 column 0 rowspan 1 columnspan 2
        self.dockwidget.filtervalueGridLayout.addLayout(self.hbox, rownum, 0, 1, 2)
    except Exception as e:
        QgsMessageLog.logMessage('addFiltervalueGeometry %s' % e, 'Radwegweiser lokal', Qgis.Info)  


def selectGeometryClicked(self, identifyPointEmitPoint, checked):
    """Toolbutton zum Festlegen einer Abfragegeometrie wurde getoggelt."""
    if checked:
        modifiers = QApplication.keyboardModifiers()
        if modifiers == Qt.ControlModifier:
            clearPointFilterLayer(self)
        self.iface.mapCanvas().setMapTool(identifyPointEmitPoint)
        # QgsMessageLog.logMessage('self.iface.mapCanvas().setMapTool(identifyPointEmitPoint)', 'Radwegweiser lokal', Qgis.Info) 
        # maptool in rik registrieren, damit es bei plugin entfernen gecleart werden kann
    else:
        self.iface.mapCanvas().unsetMapTool(identifyPointEmitPoint)
        # QgsMessageLog.logMessage('self.iface.mapCanvas().unsetMapTool(identifyPointEmitPoint)', 'Radwegweiser lokal', Qgis.Info) 

def selectGeometryPoint(self, pt, name, identifyPointEmitPoint, lblGeom):
    """Auf das Klicken eines Punktes in der Karte reagieren."""
    try:
        self.filterArgs[name] = 'SetSrid(ST_MakePoint(%s, %s),25832)' % (pt.x(), pt.y())
        lblGeom.setText('x=%s, y=%s' %  (pt.x(), pt.y()))
        # QgsMessageLog.logMessage('selectGeometryPoint %s' % ('SetSrid(ST_MakePoint(%s, %s),25832)' % (pt.x(), pt.y())), 'Radwegweiser lokal', Qgis.Info) 
        self.iface.mapCanvas().unsetMapTool(identifyPointEmitPoint)
        addPointToFilterLayer(self, pt.x(), pt.y())
    except Exception as e:
        QgsMessageLog.logMessage('Fehler bei selectGeometryPoint: %s' % e, 'Radwegweiser lokal', Qgis.Critical)              


def clearLayerFilter(self, layername):
    try:
        lyr = getLayerByName(self, layername)
        if lyr is not None:
            lyr.setSubsetString('')
            self.iface.layerTreeView().refreshLayerSymbology(lyr.id())
        return True    
    except Exception as e:
        QgsMessageLog.logMessage('clearLayerFilter ist gescheitert: %s' % e, 'Radwegweiser lokal', Qgis.Critical) 
        return False

def clearPointFilterLayer(self):
    filterLayer = None
    for lyr in list(QgsProject.instance().mapLayers().values()):
        if isinstance(lyr, QgsVectorLayer):
            if 'Filter-Geometrie' in lyr.name():
                # QgsMessageLog.logMessage('Layer Filter-Geometrie vorhanden: %s' % lyr.name(), 'Radwegweiser lokal', Qgis.Info)
                filterLayer = lyr
                break
    if filterLayer is not None:
        filterLayer.startEditing()
        request = QgsFeatureRequest()
        for feature in filterLayer.getFeatures(request):
            filterLayer.deleteFeature(feature.id())
        filterLayer.commitChanges()

def addPointToFilterLayer(self, x, y):
    filterLayer = None
    for lyr in list(QgsProject.instance().mapLayers().values()):
        if isinstance(lyr, QgsVectorLayer):
            if 'Filter-Geometrie' in lyr.name():
                # QgsMessageLog.logMessage('Layer Filter-Geometrie vorhanden: %s' % lyr.name(), 'Radwegweiser lokal', Qgis.Info)
                filterLayer = lyr
                break
    if filterLayer is None:
        filterLayer = QgsVectorLayer("Point?crs=epsg:25832", "Filter-Geometrie", "memory")
        QgsProject.instance().addMapLayer(filterLayer)
        # QgsMessageLog.logMessage('Layer Filter-Geometrie nicht vorhanden: %s' % filterLayer.name(), 'Radwegweiser lokal', Qgis.Info)

    point = QgsFeature()
    point.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(x,y)))
    filterLayer.startEditing()
    filterLayer.addFeatures([point])
    filterLayer.select(point.id())
    filterLayer.commitChanges()


def filterValueGeometryChanged(self, checkState, name, asis=bool):
    # QgsMessageLog.logMessage('filterValueGeometryChanged: checkState %s feld %s' % (checkState, name) , 'Radwegweiser lokal', Qgis.Info)   
    self.filterArgs[name] =  True if checkState == Qt.Checked else False


def deleteItemsOfLayout(layout):
     if layout is not None:
         while layout.count():
             item = layout.takeAt(0)
             widget = item.widget()
             if widget is not None:
                 widget.setParent(None)
             else:
                 deleteItemsOfLayout(item.layout())

def insertLayerFields(self, query, args):
    try:
        # print query % args
        conn = connector.Connection()
        cur = conn.con.cursor()
        execute_values(cur, query, args)
        conn.con.commit()
        return True
    except sqlite3.DatabaseError as e:
        QApplication.restoreOverrideCursor()
        QMessageBox.critical(self.dockwidget, self.dockwidget.windowTitle(), '%s' % e)
    except SystemError as e:
        QApplication.restoreOverrideCursor()
        QMessageBox.critical(self.dockwidget, self.dockwidget.windowTitle(), '%s' % e)
    except AssertionError as e:
        QApplication.restoreOverrideCursor()
        QMessageBox.warning(self.dockwidget, self.dockwidget.windowTitle(), '%s' % e)
    finally:
        QApplication.restoreOverrideCursor()
        if conn:
            try:
                cur.close()
                conn.close()
            except Exception as e:
                QMessageBox.critical(self.dockwidget, self.dockwidget.windowTitle(), 'Server closed the connection unexpectedly')   


def runQuery(self, query, args):
    try:
        #print query % args
        conn = sqlite3.connect(self.dbpfad)
        # conn = spatialite.connect(self.dbpfad)
        cur = conn.cursor()
        QgsMessageLog.logMessage('runQuery: %s %s' % (query, args), 'Radwegweiser lokal', Qgis.Info)
        cur.execute(query, args)
        conn.commit()
        # return rowdict
        return True

    except sqlite3.DatabaseError as e:
        QApplication.restoreOverrideCursor()
        QMessageBox.critical(self.dockwidget, self.dockwidget.windowTitle(), '%s' % e)
        QgsMessageLog.logMessage('sqlite3.DatabaseError: %s' % e, 'Radwegweiser lokal', Qgis.Critical)
    except SystemError as e:
        QApplication.restoreOverrideCursor()
        QMessageBox.critical(self.dockwidget, self.dockwidget.windowTitle(), '%s' % e)
    except AssertionError as e:
        QApplication.restoreOverrideCursor()
        QMessageBox.warning(self.dockwidget, self.dockwidget.windowTitle(), '%s' % e)
    finally:
        QApplication.restoreOverrideCursor()
        if conn:
            try:
                conn.close()
            except Exception as e:
                QMessageBox.critical(self.dockwidget, self.dockwidget.windowTitle(), 'Server closed the connection unexpectedly')     

def runInsertQuery(self, query, args):
    try:
        # print query % args
        conn = spatialite.connect(self.dbpfad)
        cur = conn.cursor()
        QgsMessageLog.logMessage('runQuerySelect: %s %s' % (query, args), 'Radwegweiser lokal', Qgis.Critical)
        cur.execute(query, args)
        conn.commit()
        newid = cur.lastrowid
        return newid

    except sqlite3.DatabaseError as e:
        QApplication.restoreOverrideCursor()
        QMessageBox.critical(self.dockwidget, self.dockwidget.windowTitle(), '%s' % e)
    except SystemError as e:
        QApplication.restoreOverrideCursor()
        QMessageBox.critical(self.dockwidget, self.dockwidget.windowTitle(), '%s' % e)
    except AssertionError as e:
        QApplication.restoreOverrideCursor()
        QMessageBox.warning(self.dockwidget, self.dockwidget.windowTitle(), '%s' % e)
    except Exception as e:
        QgsMessageLog.logMessage('Fehler runInsertQuery: %s' % e, 'Radwegweiser lokal', Qgis.Critical)
    finally:
        QApplication.restoreOverrideCursor()
        if conn:
            try:
                cur.close()
                conn.close()
            except Exception as e:
                QMessageBox.critical(self.dockwidget, self.dockwidget.windowTitle(), 'runInsertQuery: Server closed the connection unexpectedly')      

def dict_factory(cursor, row):
    fields = [column[0] for column in cursor.description]
    return {key: value for key, value in zip(fields, row)}


def runQuerySelect(self, query, args):
    try:
        conn = spatialite.connect(self.dbpfad)
        # conn = sqlite3.connect(self.dbpfad)
        cur = conn.cursor()
        # https://docs.python.org/3/library/sqlite3.html?highlight=row_factory#sqlite3-howto-row-factory
        cur.row_factory = dict_factory
        QgsMessageLog.logMessage('runQuerySelect: %s %s' % (query, args), 'Radwegweiser lokal', Qgis.Info)
        cur.execute(query, args)
        # return rowdict
        results = cur.fetchall()
        return results


    except sqlite3.DatabaseError as e:
        QApplication.restoreOverrideCursor()
        QMessageBox.critical(self.dockwidget, self.dockwidget.windowTitle(), '%s' % e)
        QgsMessageLog.logMessage('sqlite3.DatabaseError: %s' % e, 'Radwegweiser lokal', Qgis.Critical)
    except SystemError as e:
        QApplication.restoreOverrideCursor()
        QMessageBox.critical(self.dockwidget, self.dockwidget.windowTitle(), '%s' % e)
    except AssertionError as e:
        QApplication.restoreOverrideCursor()
        QMessageBox.warning(self.dockwidget, self.dockwidget.windowTitle(), '%s' % e)
    except Exception as e:
        QgsMessageLog.logMessage('Fehler runQuerySelect: %s' % e, 'Radwegweiser lokal', Qgis.Critical)
    finally:
        QApplication.restoreOverrideCursor()
        if conn:
            try:
                cur.close()
                conn.close()
            except Exception as e:
                QMessageBox.critical(self.dockwidget, self.dockwidget.windowTitle(), 'runQuerySelect: Server closed the connection unexpectedly')      

def runQuerySelectSingle(self, query, args):
    """Führt query mit args aus und gibt einen Wert (erster Datensatz) zurück."""
    try:

        # with spatialite.connect(self.dbpfad) as db:
        #     print(db.execute('SELECT spatialite_version()').fetchone()[0])
        #     QgsMessageLog.logMessage('spatialite_version: %s' % db.execute('SELECT spatialite_version()').fetchone()[0], 'Radwegweiser lokal', Qgis.Info)


        conn = spatialite.connect(self.dbpfad)
        cur = conn.cursor()
        # QgsMessageLog.logMessage('runQuerySelectSingle: %s %s' % (query, args), 'Radwegweiser lokal', Qgis.Info)
        cur.execute(query, args)
        value = cur.fetchone()
        return value[0]    
    except sqlite3.DatabaseError as e:
        QgsMessageLog.logMessage('Fehler: %s' % e, 'Radwegweiser lokal', Qgis.Critical)
    except SystemError as e:
        QgsMessageLog.logMessage('Fehler: %s' % e, 'Radwegweiser lokal', Qgis.Critical)
    except AssertionError as e:
        QgsMessageLog.logMessage('Fehler: %s' % e, 'Radwegweiser lokal', Qgis.Critical)
    finally:
        QApplication.restoreOverrideCursor()
        try:
            cur.close()
            conn.close()
        except:
            QgsMessageLog.logMessage('Server closed the connection unexpectedly.', 'Radwegweiser lokal', Qgis.Critical)


def setStartPoint(geomType, args):
    if geomType == 'ST_MultiLineString':
        args['startpoint'] = "ST_StartPoint(ST_GeometryN(%(geometry)s, 1))" % args
    elif geomType == 'ST_LineString':
        args['startpoint'] = "ST_StartPoint(%(geometry)s)" % args


def setEndPoint(geomType, args):
    if geomType == 'ST_MultiLineString':
        args['endpoint'] = "ST_EndPoint(ST_GeometryN(%(geometry)s, 1))" % args
    elif geomType == 'ST_LineString':
        args['endpoint'] = "ST_EndPoint(%(geometry)s)" % args


def setTransformQuotes(args):
    if args['srid'] > 0 and args['canvas_srid'] > 0:
        args['transform_s'] = "ST_Transform("
        args['transform_e'] = ", %(canvas_srid)d)" % args
    else:
        args['transform_s'] = ""
        args['transform_e'] = ""


def selectGefundenenLayer(self, layer_id):
    try:
        for index in range(self.layerTableModel.rowCount()):
            record = self.layerTableModel.record(index)
            if record.value("fid") == layer_id:
                # new_index = self.layerTableModel.createIndex(index, 1)
                QgsMessageLog.logMessage('selectGefundenenLayer: %s' % layer_id, 'Radwegweiser lokal', Qgis.Critical) 
                self.dockwidget.layerTableView.selectRow(index)
                self.iface.messageBar().pushMessage("Layer wurde gefunden und ausgewählt", Qgis.Warning, 5)
                break
    except Exception as e:
        QgsMessageLog.logMessage('Fehler in selectGefundenenLayer: %s' % e, 'Radwegweiser lokal', Qgis.Critical) 


def selectGefundenenPfosten(self, pfosten_id):
    try:
        for index in range(self.pfostenTableModel.rowCount()):
            record = self.pfostenTableModel.record(index)
            if record.value("fid") == pfosten_id:
                # new_index = self.pfostenTableModel.createIndex(index, 1)
                QgsMessageLog.logMessage('selectGefundenenPfosten: %s' % pfosten_id, 'Radwegweiser lokal', Qgis.Critical) 
                self.dockwidget.pfostenTableView.selectRow(index)
                self.iface.messageBar().pushMessage("Pfosten wurde gefunden und ausgewählt", Qgis.Warning, 5)
                break
    except Exception as e:
        QgsMessageLog.logMessage('Fehler in selectGefundenenPfosten: %s' % e, 'Radwegweiser lokal', Qgis.Critical) 
        

def getUserName(self):
    authcfg = QSettings().value("PostgreSQL/connections/%s/authcfg" % self.CONNECTION)
    qgs_auth_method_config = QgsAuthMethodConfig()
    qgs_auth_manager_instance = QgsApplication.authManager()
    qgs_auth_manager_instance.loadAuthenticationConfig(authcfg, qgs_auth_method_config, True)
    username = qgs_auth_method_config.configMap()['username']
    return username


def getStringValue(settings, key, value):
    return settings.value(key, value, type=str)


def getBoolValue(settings, key, value):
    return settings.value(key, value, type=bool)


def isQGISv1():
    return Qgis.QGIS_VERSION_INT < 10900


def getDestinationCrs(mapRenderer):
    if isQGISv1():
        return mapRenderer.destinationSrs()
    else:
        return mapRenderer.destinationCrs()


def getCanvasSrid(crs):
    if isQGISv1():
        return crs.epsg()
    else:
        return crs.postgisSrid()


def createFromSrid(crs, srid):
    if isQGISv1():
        return crs.createFromEpsg(srid)
    else:
        return crs.createFromSrid(srid)

def getRubberBandType(isPolygon):
    if isQGISv1():
        return isPolygon
    else:
        if isPolygon:
            return QgsWkbTypes.PolygonGeometry
        else:
            return QgsWkbTypes.LineGeometry


def getLayer(self, tablename):
    layer = None
    for lyr in list(QgsProject.instance().mapLayers().values()):
        if isinstance(lyr, QgsVectorLayer):
            if (tablename in lyr.source()):
                layer = lyr
    return layer

def getLayerByName(self, layername):
    layer = None
    layers = QgsProject.instance().mapLayersByName(layername)
    if layers: 
        layer = layers[0]
    return layer


def getWFSLayer(self, tablename):
    layer = None
    for lyr in list(QgsProject.instance().mapLayers().values()):
        if isinstance(lyr, QgsVectorLayer):
            if (tablename in lyr.source()):
                layer = lyr
    return layer

def getSchildFuerWegweiser(self, wegweiser_id):
    """Ermittelt den Schild-Index für einen neuen Inhalt auf einem Wegweiser."""
    query = "SELECT max(schild_nr) FROM inhalt WHERE wegweiser_id = :wegweiser_id;" 
    args = {'wegweiser_id': wegweiser_id}
    schild_nr = runQuerySelectSingle(self, query, args)
    return schild_nr

def getZeileFuerSchild(self, wegweiser_id, schild_nr):
    """Ermittelt den Zeilen-Index für einen Inhalt auf einem Wegweiser."""
    query = "SELECT max(zeile) FROM inhalt WHERE wegweiser_id = :wegweiser_id AND schild_nr = :schild_nr ;" 
    args = {'wegweiser_id': wegweiser_id, 'schild_nr': schild_nr}
    zeile = runQuerySelectSingle(self, query, args)
    return zeile

def getEinschubFuerWegweiser(self, wegweiser_id):
    """Ermittelt die Einschub-Position für einen neuen Einschub an einem Wegweiser."""
    query = "SELECT max(position_nr) FROM wegweiser_einschub_assoc WHERE wegweiser_id = :wegweiser_id ;" 
    args = {'wegweiser_id': wegweiser_id}
    position_nr = runQuerySelectSingle(self, query, args)
    if position_nr is None:
        position_nr = 0
    return position_nr

def getSchildLfdNr(self, wegweiser_id):
    """Ermittelt die laufende Nummer für einen neuen Inhalt auf einem Wegweiser."""
    query = "SELECT count(schild_nr) FROM inhalt WHERE wegweiser_id = :wegweiser_id;" 
    args = {'wegweiser_id': wegweiser_id}
    lfdnr = runQuerySelectSingle(self, query, args)
    return lfdnr + 1

def getTruetypeZeichen(self):
    """Holt eine Liste der TrueType-Zeichen für Pfeile und Piktogramme (bezeichnung, TrueType-Zeichen für QML)."""

    query = QSqlQuery(self.db)
    abfrage = "SELECT bezeichnung, zeichen FROM truetype_zeichen;" 
    query.exec(abfrage)
    while query.next():
        # QgsMessageLog.logMessage('getTruetypeZeichen ergebnis (%s) %s' % (query.value(0), query.value(1)), 'Radwegweiser lokal', Qgis.Info)
        self.piktogramme[query.value(0)] = query.value(1)

    return True

