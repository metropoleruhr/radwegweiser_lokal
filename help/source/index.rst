.. Radwegweisungskataster documentation master file, created by
   sphinx-quickstart on Sun Aug 05 17:11:03 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Dokumentation Radwegweisungskataster
============================================

.. toctree::
   :maxdepth: 3

   einfuehrung/index.rst
   installation/index.rst
   uebersicht/index.rst
   plugin/index.rst



