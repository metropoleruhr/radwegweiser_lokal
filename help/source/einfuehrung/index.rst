.. _einfuehrung:

Einführung
############

Dieses Handbuch dokumentiert die Werkzeuge und Vorgehensweisen zur Pflege 
eines Radwegweisungskatasters mittels QGIS.

.. toctree::
   :maxdepth: 3

   ausgangslage.rst
   loesungsweg.rst
   begriffe.rst

.. figure:: img/reiter_wegweiser1.png
       :figclass: align-center
       :scale: 70%

       Bedienoberfläche für Radwegweiser
