# -*- coding: utf-8 -*-

from PyQt5.QtCore import Qt, QRect, QEvent, QObject, QSettings, QFileInfo, QThread, QSortFilterProxyModel, QCoreApplication
from PyQt5.QtWidgets import QApplication, QComboBox, QCompleter, QHeaderView, QAction, QDataWidgetMapper, \
    QFileDialog, QPushButton, QStyleOptionButton, QStyle, QItemDelegate, QStyledItemDelegate, QMessageBox, QShortcut 
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QColor, QIcon, QKeySequence

class DateFormatDelegate(QStyledItemDelegate):
    def __init__(self, date_format):
        QStyledItemDelegate.__init__(self)        
        self.date_format = date_format

    def displayText(self, value, locale):
        return value.toString(self.date_format)