.. _begriffe_uebersicht:

Begriffe
################################################


.. _begriff_knoten:

**Knoten**

Ein Knoten ist ein im Radwegenetz verorteter Punkt, dem Pfosten zugeordnet werden können.
Knoten haben eine Knoten-Nr., eine Bezeichnung und ggf. weitere textliche Bemerkungen.



.. _begriff_pfosten:

**Pfosten**

Pfosten sind die physisch im Feld auffindbaren Pfosten, die als Punkt 
möglichst exakt an diesem Standort verortet werden.
Pfosten haben diverse Eigenschaften und sind i.d.R. einem Knoten zuzuordnen.



.. _begriff_wegweiser:

**Wegweiser**

Wegweiser stellen die an einem Pfosten montierten Einheiten dar.
Sie werden z.B. nach Pfeilwegweiser, Tabellenwegweiser und Zwischenwegweiser unterschieden.
Wichtige Eigenschaften (neben dem Typ) sind die Richtung und ggf. zusätzliche Drehung, 
die Abfolge am Pfosten und die Pfeilart. |br|
Ein Wegweiser kann aus mehreren Schildern bestehen (wenn diese vom gleichen Typ sind
und in die gleiche Richtung weisen). Es wird je Wegweiser aber nur eine Grafikdatei erstellt.
Wegweisern können zusätzliche Einschübe zugeordnet werden.

.. _begriff_schild:

**Schild**

Schildinhalte sind die Orts- und Entfernungsangaben sowie ggf. zusätzliche Ziel- und Streckenpiktogramme.
Es werden max. 2 Inhalte (Zeilen) auf einem Schild angeordnet. Bei weiteren Inhalten werden
zusätzliche Schilder in den Wegweiser integriert. 

.. _begriff_einschub:

**Einschub**

Einschübe sind quadratische Schilder, die in einer bestimmten Reihenfolge Wegweisern 
zugeordnet werden können. Sie werden unterhalb des Wegweisers montiert und verweisen 
auf Knotenpunkte und/oder auf Themenrouten. Je Knotenpunkt bzw. Themenroute ist eine
entsprechende Grafikdatei im Dateisystem bereitzustellen.  


.. _fig_wegweiser_beispiel:

.. figure:: img/wegweiser_34593.png
    :figclass: align-center
    :width: 300pt
        
    |br|
    Tabellenwegweiser mit zwei Schildern, einem Zielpiktogramm |br| und drei Einschüben (davon zwei Themenrouten)

.. _begriff_zielpiktogramm:

**Zielpiktogramm**

Zielpiktogramme sind kleine graphische Symbole, die vor der Ortsangabe 
auf einem Schild angezeigt werden können. Sie informieren über die Eigenschaften an
diesem Ort, z.B. ob dort ein S-Bahn-Haltepunkt oder eine Fähre ist.

Aktuell sind folgende Zielpiktogramme hinterlegt:

.. raw:: html

    <span class="hbr">- Bahnhof Ù</span></br>
    <span class="hbr">- S-Bahn ¼</span></br>
    <span class="hbr">- Fähre Ú</span></br>
    <span class="hbr">- Radstation Þ</span></br>
    <span class="hbr">- Freibad Û</span></br>
    <span class="hbr">- Hallenbad Ý</span></br>
    <span class="hbr">- Restaurant ã</span></br>
    <span class="hbr">- Touristinfo Õ</span></br>
    <span class="hbr">- Bike+Ride ®</span></br>
    <span class="hbr">- Aussichtspunkt Ø</span></br>
    <span class="hbr">- Revierrad ð</span></br>
    <span class="hbr">- UNESCO Welterbe ó</span></br>
    <span class="hbr">- Museum ñ</span></br>
    <span class="hbr">- Ruine ò</span></br>
    <span class="hbr">- Förderturm ô</span></br>
    <span class="hbr">- Jugendherberge õ</span>


.. raw:: latex

    * Bahnhof 
    * S-Bahn 
    * Fähre 
    * Radstation 
    * Freibad 
    * Hallenbad 
    * Restaurant 
    * Touristinfo 
    * Bike+Ride 
    * Aussichtspunkt 
    * Revierrad 
    * UNESCO Welterbe 
    * Museum 
    * Ruine 
    * Förderturm 
    * Jugendherberge 

|br|

.. _begriff_streckenpiktogramm:

**Streckenpiktogramm**

Streckenpiktogramme sind kleine graphische Symbole, die nach der Ortsangabe 
auf einem Schild angezeigt werden können. Sie informieren über die Eigenschaften der
ausgeschilderten Strecke, z.B. ob sie eine Radschnellverbindung oder ein Bahntrassenradweg ist.

Aktuell sind folgende Streckenpiktogramme hinterlegt:

.. raw:: html

    <span class="hbr">- Freizeitstrecke Ò</span></br>
    <span class="hbr">- Bahntrassenradweg Ó</span></br>
    <span class="hbr">- Radschnellverbindung Ô</span></br>
    <span class="hbr">- Steigung æ</span></br>
    <span class="hbr">- Gefälle ç</span></br>
    <span class="hbr">- Treppe î</span></br>

.. raw:: latex

    * Freizeitstrecke 
    * Bahntrassenradweg 
    * Radschnellverbindung 
    * Steigung 
    * Gefälle 
    * Treppe 

|br|
