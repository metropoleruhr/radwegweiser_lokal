# -*- coding: utf-8 -*-

from builtins import range
from qgis.PyQt.QtCore import Qt
from qgis.PyQt import QtSql
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtSql import QSqlDatabase

import json
#import resources

ID, RICHTUNG, DREHUNG, PFEILART, NACHPUNKT, VPINFO_ID, PFOSTEN_ID, PFOSTEN, WEGWEISERTYP_ID, PFEILART_ID, HERKUNFT, KOMMENTAR, ABFOLGE = list(range(13))


class PfostenWegweiserTableModel(QtSql.QSqlRelationalTableModel):

    def __init__(self, parent=None, db=QSqlDatabase()):
        super(PfostenWegweiserTableModel, self).__init__(parent, db)
        self.LockedColumns = [ID,VPINFO_ID]

    def flags(self, index):
        flags = QtSql.QSqlTableModel.flags(self, index)
        if index.column() not in self.LockedColumns:
            flags |= Qt.ItemIsEditable
        else:
            flags = Qt.ItemIsEnabled | Qt.ItemIsSelectable
        return flags

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return None

        if role == Qt.TextAlignmentRole:
            if index.column() in (ID, DREHUNG, PFEILART, ABFOLGE):
                return Qt.AlignCenter | Qt.AlignVCenter
            else:
                return Qt.AlignLeft | Qt.AlignVCenter

        # if index.column() == ID:
        #     if role == Qt.ToolTipRole:
        #         quelle = self.record(index.row()).value("source")
        #         return quelle

        if index.column() == PFEILART:
            if role == Qt.DisplayRole:
                pfeilart = str(self.record(index.row()).value("pfeilart"))
                if pfeilart in ('!'):
                    anzeige = 'geradeaus'
                elif pfeilart in ('B'):
                    anzeige = 'links'
                elif pfeilart in ('b'):
                    anzeige = 'rechts'
                elif pfeilart in ('A'):
                    anzeige = 'schräg links'
                elif pfeilart in ('a'):
                    anzeige = 'schräg rechts'
                elif pfeilart in ('#'):
                    anzeige = 'Versatz links'
                elif pfeilart in ('$'):
                    anzeige = 'Versatz rechts'
                elif pfeilart == 'E':
                    anzeige = 'gebogen links'
                elif pfeilart == 'e':
                    anzeige = 'gebogen rechts'
                elif pfeilart == 'I':
                    anzeige = 'wenden links'
                elif pfeilart == 'i':
                    anzeige = 'wenden rechts'
                elif pfeilart == 'Â':
                    anzeige = 'nach Kreis geradeaus'
                elif pfeilart == 'À':
                    anzeige = 'im Kreis 1.Abzweig'
                elif pfeilart == 'Á':
                    anzeige = 'im Kreis 3.Abzweig'
                elif pfeilart == '':
                    anzeige = 'geradeaus'
                else:
                    anzeige = 'kein Pfeil'
                return anzeige
            elif role == Qt.ToolTipRole:
                typ = self.record(index.row()).value("pfeilart")
                # return "Metadaten sind vorhanden." if (mv == True) else "Metadaten müssen noch geladen werden"
                return typ


        if QtSql.QSqlTableModel.data(self, index, role) == None:
            return None
        else:
            return QtSql.QSqlTableModel.data(self, index, role)

    def setData(self, index, value, role=Qt.EditRole):
        if value == '' or value == None:
            QtSql.QSqlTableModel.setData(self, index, None, role)
        else:
            QtSql.QSqlTableModel.setData(self, index, value, role)

        self.dirty = True
        # self.dataChanged.emit(index, index)
        return True
