.. _gui_fotos:

Unterreiter Fotos
**********************

Der **Unterreiter Fotos** listet zum ausgewählten Pfosten die Fotos 
zur Dokumentation der Wegweiser auf.
Die Tabelle der Fotos enthält die Spalten (eindeutige) **ID**, **Titel** und **Datei**. 

  .. figure:: img/reiter_fotos1.png
       :figclass: align-center
       :scale: 80%


Unterhalb der Tabelle der Fotos befinden sich die folgende **Schaltflächen**:

* |I1| **löscht** das ausgewählte Foto nach Bestätigung einer Sicherheitsabfrage |br| 
* |I2| **Foto zuordnen** öffnet einen Dateiauswahldialog zur Auswahl eines Fotos,
  dass dem Pfosten zugeordnet werden soll. Für jeden Pfosten gibt es einen Unterordner 
  im Foto-Verzeichnis (benannt nach der Pfosten-ID), in den das ausgewählte Foto kopiert wird.

* |I4| **speichern** von Änderungen des Foto-Titels. |br| 
  Der Dateipfad kann nicht geändert werden. |br|
  Schaltfläche **Öffnen** öffnet das Photo mit der zugeordneten Anwendung.  
* |I5| Änderungen an Eigenschaften des Fotos **verwerfen** und Formular neu füllen.


.. |I1| image:: img/delete.png
.. |I2| image:: img/open.png
.. |I3| image:: img/add.png
.. |I4| image:: img/save.png
.. |I5| image:: img/refresh.png

