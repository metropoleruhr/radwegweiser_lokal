# -*- coding: utf-8 -*-

from builtins import range
from qgis.PyQt.QtCore import Qt
from qgis.PyQt import QtSql
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtSql import QSqlDatabase

import json
#import resources

ID, GEOM, VPINFO_ID, RVN_ID, KNOTEN_NR, BEZEICHNUNG, BEMERKUNG, KNOTENTYP_ID, GEMEINDE_ID, BAULAST_ID = list(range(10))


class KnotenTableModel(QtSql.QSqlRelationalTableModel):

    def __init__(self, parent=None, db=QSqlDatabase()):
        super(KnotenTableModel, self).__init__(parent, db)
        self.LockedColumns = [ID]

    def flags(self, index):
        flags = QtSql.QSqlTableModel.flags(self, index)
        if index.column() not in self.LockedColumns:
            flags |= Qt.ItemIsEditable
        else:
            flags = Qt.ItemIsEnabled | Qt.ItemIsSelectable
        return flags

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return None

        if role == Qt.TextAlignmentRole:
            if index.column() in (ID, KNOTEN_NR):
                return Qt.AlignCenter | Qt.AlignVCenter
            else:
                return Qt.AlignLeft | Qt.AlignVCenter

        if index.column() == ID:
            if role == Qt.ToolTipRole:
                quelle = self.record(index.row()).value("source")
                return quelle

        # if index.column() == TYP:
        #     if role == Qt.DisplayRole:
        #         return ''
        #     elif role == Qt.DecorationRole:
        #         typ = self.record(index.row()).value("typ")
        #         if typ == 'Raster':
        #             icon = QIcon(":/plugins/datenkatalog/icons/mIconRasterLayer.png")
        #         elif typ == 'Point':
        #             icon = QIcon(":/plugins/datenkatalog/icons/mIconPointLayer.png")
        #         elif typ == 'Line':
        #             icon = QIcon(":/plugins/datenkatalog/icons/mIconLineLayer.png")
        #         elif typ == 'Polygon':
        #             icon = QIcon(":/plugins/datenkatalog/icons/mIconPolygonLayer.png")
        #         elif typ == 'No geometry':
        #             icon = QIcon(":/plugins/datenkatalog/icons/mIconTableLayer.png")
        #         else:
        #             icon = QIcon(":/plugins/datenkatalog/icons/star_red.png")                    
        #         return icon
        #     elif role == Qt.ToolTipRole:
        #         typ = self.record(index.row()).value("typ")
        #         # return "Metadaten sind vorhanden." if (mv == True) else "Metadaten müssen noch geladen werden"
        #         return typ


        if QtSql.QSqlTableModel.data(self, index, role) == None:
            return None
        else:
            return QtSql.QSqlTableModel.data(self, index, role)

    def setData(self, index, value, role=Qt.EditRole):
        if value == '' or value == None:
            QtSql.QSqlTableModel.setData(self, index, None, role)
        else:
            QtSql.QSqlTableModel.setData(self, index, value, role)

        self.dirty = True
        # self.dataChanged.emit(index, index)
        return True
