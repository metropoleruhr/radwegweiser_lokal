.. _vorgehensweise:

Vorgehensweisen
*************************

Einen Knoten anlegen 
;;;;;;;;;;;;;;;;;;;;;;;;;

Um einen neuen Knoten zu ergänzen, wird die Schaltfläche **Knoten editieren** betätigt.
Damit wird der **Layer Knoten** im Layerbaum aktiviert
und der Bearbeitungsmodus auf **Editieren** umgeschaltet.
(Zudem werden etwaige Zugriffssperren auf die SpatiaLite-Datenbank 
durch eine Aktualisierung der Datenbankverbindung aufgehoben.)

Mit dem Werkzeug **Punktobjekt hinzufügen** wird im Wegenetz der Knoten positioniert.
Es öffnet sich der **Dialog Knoten - Objektattribute** in dem die Knoten-Nr. und Bezeichnung
eingegeben wird. **OK** schließt den Dialog. 

.. figure:: img/neuer_knoten1.png
       :figclass: align-left
       :scale: 70%
       
       *Knoten* als Punktobjekt im Layer hinzufügen

Klick auf **Layeränderungen speichern** speichert den neuen Knoten und listet ihn in der Tabelle 
der Pfosten auf. Der Bearbeitungsmodus des Layers sollte beendet werden. 

  .. figure:: img/neuer_knoten1a.png
       :figclass: align-center
       :scale: 80%

       *Knoten* wird in der Bedienoberfläche angezeigt.

.. _knoten_loeschen:

Einen Knoten löschen
;;;;;;;;;;;;;;;;;;;;;;;;;

Im Regelfall sollte es selten vorkommen, dass ein Knoten wieder gelöscht werden soll.
Um einen bestehenden Knoten zu löschen, wird dieser in der Tabelle der Knoten ausgewählt
und die Schaltfläche |I3| **Knoten löschen** betätigt. |br|
Es wird eine Sicherheitsabfrage angezeigt, die mit 'Ja' bestätigt wird.
Danach werden etwaige Verweise von Pfosten auf diesen Knoten zurückgesetzt
und der Knoten gelöscht.

.. figure:: img/knoten_loeschen1.png
       :figclass: align-center
       :scale: 70%
       
       *Knoten* löschen: Sicherheitsabfrage

Einen Pfosten anlegen 
;;;;;;;;;;;;;;;;;;;;;;;;;

Um einen neuen Pfosten zu ergänzen, wird die Schaltfläche **Pfosten editieren** betätigt.
Damit wird der **Layer Pfosten (Editieren)** im Layerbaum aktiviert
und der Bearbeitungsmodus auf **Editieren** umgeschaltet.
(Zudem werden etwaige Zugriffssperren auf die SpatiaLite-Datenbank 
durch eine Aktualisierung der Datenbankverbindung aufgehoben.)

.. figure:: img/neuer_pfosten0.png
       :figclass: align-left
       :scale: 70%
       
       *Pfosten* als Punktobjekt im Layer hinzufügen

Mit dem Werkzeug **Punktobjekt hinzufügen** wird anschließend in der Karte der Pfosten positioniert.
Es öffnet sich der **Dialog Pfosten (Editieren) - Objektattribute** in dem mindestens die Pfosten-Nr. 
eingegeben wird. **OK** schließt den Dialog. 

.. figure:: img/neuer_pfosten1.png
       :figclass: align-left
       :scale: 70%
       
       *Pfosten* als Punktobjekt im Layer hinzufügen

.. hint:: 
    Die Knoten-ID des neuen Pfostens ist auf den aktuell ausgewählten Knoten voreingestellt. |br| |nl|
    Um einen Pfosten zu löschen steht auch hier eine Schaltfläche |I3| zur Verfügung.

Klick auf **Layeränderungen speichern** speichert den neuen Pfosten und listet ihn
in der Tabelle der Pfosten auf. Der Bearbeitungsmodus des Layers sollte beendet werden.

.. figure:: img/neuer_pfosten1a.png
       :figclass: align-center
       :scale: 70%

       *Pfosten* wird in Bedienoberfläche und Karte angezeigt.


.. figure:: img/neuer_pfosten2.png
       :figclass: align-left
       :scale: 80%

       *Pfosten-Details* ausfüllen.

Anschließend füllen wir die Pfosten-Details aus z.B. Status, Material, Eigentümer.

|np|

Es können zudem Pfosten-Zugriffsrechte (also Zuständigkeiten) definiert werden.
Diese haben aber bislang keine weitere Bedeutung.

.. figure:: img/empty.png
       :figclass: align-center
       :width: 400px
       :height: 1px

Wegweiser am Pfosten ergänzen
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Durch Klick auf |I1| im **Reiter Wegweiser** kann dem aktuell ausgewählten Pfosten ein Wegweiser hinzugefügt werden.
Dieser wird in der Tabelle der Wegweiser ergänzt und ausgewählt.

.. figure:: img/neuer_wegweiser1.png
       :figclass: align-left
       :scale: 80%

       Einen ersten Wegweiser hinzufügen.

Hier wählen wir zuerst den **Wegweisertyp** aus. |br| 
Ist es ein Pfeilwegweiser, so wählen wir die **Richtung** aus, **in die** der Pfeilwegweiser zeigen soll. 
Ist es ein Tabellen- oder Zwischenwegweiser, die Richtung, **aus der** der Wegweiser sichtbar ist.

.. figure:: img/neuer_wegweiser2.png
       :figclass: align-center
       :scale: 80%

Neben der Richtungsauswahl kann (wenn gewollt) ein zusätzlicher Wert (in Grad) im Feld **Drehung**
eingegeben werden, der von der Richtung abgezogen wird.
Mit jeder Änderung werden die Wegweiser-Layer in der Karte entsprechend gedreht. 

Die Auswahl der **Pfeilart** schließt die Einstellungen zum Wegweiser vorerst ab.
Im Reiter Wegweiser-Grafik ist bereits der Tabellenwegweiser mit dem Pfeil sichtbar. 

.. figure:: img/neuer_wegweiser2a.png
       :figclass: align-right
       :scale: 60%

Doch es fehlen die Schildinhalte. |br| |br| 
Durch Klick auf |I1| im **Reiter Schildinhalte** wird eine neue Zeile eingetragen.

  .. figure:: img/neuer_wegweiser3.png
       :figclass: align-center
       :scale: 80%

|np|

Das **Textfeld Hinweis** ist bereits ausgewählt, so dass direkt der Zielhinweis (z.B. Essen-Heisingen)
eingetragen werden kann. Mit der Tabulator-Taste springen wir in das Feld **Entfernung** und geben
diese (z.B. 1,4) ein.

Wählen wir nun noch als :ref:`Ziel-Piktogramm-1 <begriff_zielpiktogramm>` **Restaurant** und als :ref:`Streckenpiktogramm-1 <begriff_streckenpiktogramm>` **Freizeitstrecke** aus,
so sieht die Wegweisergrafik schon so aus: 

.. figure:: img/neuer_wegweiser3a.png
       :figclass: align-right
       :scale: 70%

Nachdem wir einen weiteren Inhalt (Essen-Rüttenscheid: Schild 0 an Zeile 1) und einen 
dritten (Hattingen: Schild 1 Zeile 0) ergänzt haben, sieht die Wegweisergrafik so aus: 

.. figure:: img/neuer_wegweiser3b.png
       :figclass: align-left
       :scale: 70%

Gibt es an dem Pfosten weitere Wegweiser, die nicht nach links (Pfeilart) weisen 
oder aus einer anderen Richtung sichtbar sind, so erstellen wir einen weiteren Wegweiser,
setzen Richtung und Pfeilart und ergänzen seine Schildinhalte.


Einschübe ergänzen
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Der Reiter **Einschübe** zeigt die dem Wegweiser zugordneten :ref:`Einschübe  <begriff_einschub>` tabellarisch an.
Dies sind graphische Hinweise auf Themenrouten (z.B. den Ruhrtalradweg) und Knotenpunkte.
Angezeigt werden die eindeutige ID, die Bezeichnung des Einschubs und die Position am Wegweiser.

.. figure:: img/neuer_einschub.png
    :figclass: align-center 
    :width: 70%

Um einen (weiteren) Einschub zu ergänzen, wählen Sie diesen zuerst im **Kombinationsfeld Einschub** 
aus und klicken dann auf die Schaltfläche |I1| **Hinzufügen**. 

|np|

.. tipp:: 
          .. figure:: img/einschub_suchen.png
               :figclass: align-right 
               :width: 70%
          
          Sie können Namensteile eines Einschubes im **Kombinationsfeld Einschub** 
          eintippen, um die Liste auf die verfügbaren Einschübe einzugrenzen. 
          Wählen Sie dann den gesuchten Einschub aus und klicken dann auf die Schaltfläche |I1| **Hinzufügen**

.. hint:: Ist der gewünschte Einschub nicht verfügbar, ist er als Grafikdatei und in der Datenbank zu ergänzen.

Der Einschub wird unmittelbar als Eintrag in der Tabelle der Einschübe aufgelistet und ausgewählt
sowie im Feld **Einschubgrafik** angezeigt.

.. figure:: img/neuer_einschub1.png
    :figclass: align-center 
    :width: 70%

Sie können die Position des Einschubs unterhalb des Wegweisers mittels Zahlenfeld Position
zwischen 1 und 6 wählen. Verliert das Feld nach Änderung den Focus (z.B. durch die TAB-Taste),
so wird die Schaltfläche |I2| **Speichern** aktiv. Klick auf diese Schaltfläche sichert die Änderung
in der Datenbank.

.. figure:: img/einschub_position1.png
    :figclass: align-center 
    :width: 60%

|np|

Die Einschubgrafik wird entsprechend positioniert.

.. figure:: img/einschub_position2.png
    :figclass: align-center 
    :width: 70%

Wollen Sie einen ausgewählten Einschub aus dem Wegweiser entfernen, so betätigen Sie die Schaltfläche |I3|
und beantworten die Sicherheitsabfrage mit 'Ja'.

.. figure:: img/einschub_loeschen1.png
    :figclass: align-center 
    :width: 80%

.. hint:: Im Reiter Einschübe wird nur die aktuell ausgewählte Einschubgrafik angezeigt.
          Alle Einschübe eines Wegweisers sehen sie im Reiter Wegweiser-Grafik. 


Wegweisergrafik prüfen und speichern
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Ist die Bearbeitung der Schildinhalte und Einschübe abgeschlossen, klicken wir auf die
Schaltfläche |I5|. 

Die Wegweiser-Grafik wird dadurch neu berechnet und im gleichnamigen Reiter angezeigt.

|np|

.. figure:: img/wegweiser_zeichnen1.png
    :figclass: align-center 
    :width: 80%

Nachdem wir die Wegweisergrafik visuell geprüft haben, können wir mit Klick auf |I6|
eine PNG-Grafik erzeugen und erstmals lokal speichern bzw. dort ersetzen. 

.. hint:: Das Plugin gibt im Protokolldialog den kompletten Dateipfad aus 
          und wechselt in den Reiter Wegweiser-Details. Hier wird die gespeicherte PNG-Grafik
          angezeigt.

.. attention:: Gibt es visuelle Unterschiede zwischen den im Reiter Wegweiser-Details 
               und dem Reiter Wegweiser-Grafik angezeigten Grafiken, so haben Sie den letzten
               Stand der Wegweiser-Grafik noch nicht im lokalen Dateisystem gespeichert.



Fotos zum Pfosten ergänzen
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Einem Pfosten und Wegweiser kann ein oder mehrere Fotos zugeordnet werden. Diese dokumentieren die reale
Situation dieses Pfostens (und seiner Wegweiser) im Gelände und werden im PDF-Katasterblatt
mit ausgegeben.

Wechseln Sie hierzu in den Reiter Fotos und betätigen die Schaltfläche |I8| **Foto zuordnen** .
Es öffnet sich ein Dateiauswahldialog, mit dem Sie eine Fotodatei (jpg- bzw. png-Format) auswählen
und mit **Öffnen** bestätigen.

.. figure:: img/foto_zuordnen1.png
    :figclass: align-center 
    :width: 90%

|br|
Es wird dann unmittelbar ein Eintrag in der Tabelle der Fotos ergänzt und das Foto
unterhalb des Formulars angezeigt. 

.. hint:: Die ausgewählte Fotodatei wird in das Verzeichnis der Pfosten-Fotos kopiert.
          Es wird für jeden Pfosten ein nach der Pfosten-ID benanntes Unterverzeichnis angelegt. 

Der Titel des Fotos wird auf "Pfosten [Pfosten-ID]"" voreingestellt. |br|
Ändern sie ihn ggf. in einen aussagekräftigeren Titel (z.B. Pfosten ESN999-1 aus Richtung Süden),
verlassen das Textfeld (z.B. mit der TAB-Taste) und **speichern** durch Klick auf |I2| unterhalb der Tabelle der Fotos.

|np|

.. figure:: img/foto_zuordnen2.png
    :figclass: align-center 
    :width: 80%

|br|
Der relative Dateipfad (ausgehend vom Verzeichnis der Pfosten-Fotos) wird lesend im Formular angezeigt.
Er kann nicht geändert werden. Durch Klicken auf die Schaltfläche **Öffnen** (neben dem Dateipfad) 
öffnen Sie das Bild mit der für diesen Dateityp festgelegten Standardsoftware.


PDF-Katasterblatt zum Knoten/Pfosten erstellen
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Die grundsätzliche Funktionsweise des PDF-Katasterblattes ist im Kapitel :ref:`QGIS-Layout <layout_uebersicht>` beschrieben.

Ist ein Pfosten ausgewählt und korrekt mit Wegweisern konfiguriert - dazu gehört auch, 
die Wegweisergrafik lokal als Grafikdatei zu speichern - so kann durch Betätigen von |I9| ein PDF-Katasterblatt
zu allen Pfosten des Knotens oder (Strg-Klick) zum aktuell ausgewählten Pfosten erstellt werden.

.. figure:: img/pdf_katasterblatt1.png
    :figclass: align-center 
    :width: 80%

Es wird ein Hinweis, dass die PDF-Erstellung angestoßen wurde,  oberhalb der Karte angezeigt.
**Warten Sie ab, bis das PDF erzeugt und z.B. in Adobe Acrobat Reader geöffnet wurde.**


.. figure:: img/pdf_katasterblatt2.png
    :figclass: align-center 
    :width: 90%

.. hint:: Das PDF-Katasterblatt für alle Pfosten eines Knotens wird nach dem Knoten bennannt 
          (Knoten-Nr zzgl. _k und Knoten-ID) das für einen Pfosten nach dem Pfosten (Knoten-Nr zzgl. _p und Pfosten-ID). 
          Die Dateien befinden sich in dem unter Einstellungen vorgegebenen Ausgabeverzeichnis für das PDF-Kataster.

|np|

.. figure:: img/pdf_katasterblatt3.png
    :figclass: align-center 
    :width: 80%

Das fertiggestellt PDF können Sie prüfen und dann ggf. zur Fortführung des Schilderkatasters 
an die zuständige Stelle senden.

|nbsp| |br|

|np|


.. |I1| image:: img/add.png
.. |I2| image:: img/save.png
.. |I3| image:: img/delete.png
.. |I4| image:: img/stylemanager.png
.. |I5| image:: img/schild_zeichnen_button.png
.. |I6| image:: img/lokal_speichern.png
.. |I7| image:: img/refresh.png
.. |I8| image:: img/open.png
.. |I9| image:: img/pdficon_small.png

