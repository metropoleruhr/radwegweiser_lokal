.. _plugin:

Erweiterung "Radwegweisung mit QGIS" installieren
*****************************************************

Die Erweiterung wird als zip-Datei bereitgestellt. 

  .. figure:: img/plugin_zip.png
       :figclass: align-center
       :scale: 80%

Wählen Sie im Hauptmenü *Erweiterungen* - **Erweiterungen verwalten und installieren**.
Es öffnet sich der Dialog **Erweiterungen**. 
Wählen Sie links den Reiter *Aus ZIP installieren* und wählen Sie die Datei radwegweiser_lokal.zip im Dateisystem aus.
Klicken Sie anschließend auf **Erweiterung installieren**. 

Es erfolgt ein Sicherheitshinweis, dass das Plugin aus einer nicht vertrauenswürdigen Quelle stammen könnte.
Wenn Sie fortfahren wollen, klicken Sie auf **Ja**.

  .. figure:: img/plugin_zip2.png
       :figclass: align-center
       :scale: 80%

Es erscheint der Hinweis, dass das Plugin erfolgreich installiert wurde:

  .. figure:: img/plugin_zip3.png
       :figclass: align-center
       :scale: 90%

Die Erweiterung wird geladen und in QGIS wird das :ref:`Bedienfeld Radwegweiser in QGIS <plugin>` sichtbar.

.. figure:: img/bedienfeld_radwegweiser_leer.png
       :figclass: align-center
       :scale: 80%

.. attention:: Beim ersten Installieren des Plugins ist das Bedienfeld leer, weil die Datenbankverbindung noch nicht eingerichtet ist.

.. hint:: Sollte das Bedienfeld einmal nicht sichtbar sein, obwohl die Erweiterung aktiviert ist, so öffnen Sie das QGIS-Kontextmenü (rechte Maustaste im QGIS-Hauptmenü) 
    und setzen das Häkchen bei **Bedienfeld Radwegweiser in QGIS**. 
    
      .. figure:: img/bedienfeld1.png
       :figclass: align-right
       :width: 180pt

.. hint:: Die Erweiterung wird im Verzeichnis python\\plugins\\radwegweiser_lokal 
    des QGIS-Profilverzeichnisses installiert. 

|np|