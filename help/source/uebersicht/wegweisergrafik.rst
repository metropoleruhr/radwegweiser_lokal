.. _wegweisergrafik_uebersicht:

Wegweisergrafik erzeugen
################################################

Aus den Eigenschaften des Wegweisers, den Schildinhalten und Einschüben wird 
mittels Qt Modeling Language (QML) und QtQuick eine Grafik erzeugt und im Reiter "Wegweiser-Grafik" angezeigt.

.. figure:: img/34599.png
    :figclass: align-right black_outline
    :width: 80

    Zwischenwegweiser

.. _wegweisertyp:

Der **Wegweisertyp** und die **Pfeilart** bestimmen das grundlegende Design der Wegweisergrafik.
Die Richtung ist bei Zwischen- und Tabellenwegweisern nur für die Darstellung in
der Karte relevant. Die Pfeilart legt bei Pfeilwegweisern fest, ob die Pfeilspitze nach links oder
rechts orientiert ist.

.. figure:: img/13194.png
    :figclass: align-left black_outline
    :width: 50%

    Pfeilwegweiser

.. figure:: img/34609.png
    :figclass: align-left black_outline
    :width: 50%

    Tabellenwegweiser


.. figure:: img/wegweiserdetails.png
    :figclass: align-center 
    :width: 80%

    Eingabe der Wegweiser-Details

|np|

Die **Schildinhalte** umfassen die einzelnen Ortsangaben nebst Entfernung. 
Wo sie in der Grafik positioniert werden, ergibt sich aus der Angabe zum Schild und zur Zeile
auf diesem Schild. Die laufende Nummer wird in der Datenbank automatisiert ergänzt.
Es können bis zu zwei :ref:`Ziel- und Streckenpiktogramme <begriff_zielpiktogramm>` je Zeile hinzugefügt werden.

.. figure:: img/schildinhalte.png
    :figclass: align-center 
    :width: 80%
    
|np|

Darüberhinaus können je Wegweiser bis zu sechs :ref:`Einschübe <begriff_einschub>` ausgewählt und unterhalb der Schildergrafik
positioniert werden.

.. figure:: img/einschuebe.png
    :figclass: align-center 
    :width: 70%

Im Ergebnis ergibt sich aus den hier dargestellten Werten folgende Wegweisergrafik:

.. figure:: img/fertige_grafik.png
    :figclass: align-right 
    :width: 60%

Die Zeichenfläche im Reiter "Wegweiser-Grafik" (QQuickWidget) dient zum Rendern der Wegweisergrafik
in eine Bilddatei ([Wegweiser-ID].png). Diese kann dann im Dateisystem (Verzeichnis radwegweiser/schild)
gespeichert werden.
Anschießend wird das Wegweiserbild im Reiter "Wegweiser-Details" angezeigt. 

.. hint:: Unterschiede zwischen angezeigtem Wegweiserbild (Reiter Wegweiser-Details) und Wegweisergrafik 
          (Reiter Wegweiser-Grafik) bedeuten, dass Sie Daten zum Wegweiser geändert aber das Wegweiserbild
          nicht erneut gespeichert haben.

