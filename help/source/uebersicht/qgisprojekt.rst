.. _qgisprojekt_layer:

Layer in der QGIS-Projektvorlage
################################################

Zur Bearbeitung der Geodaten (Knoten, Pfosten) insbesondere aber für die Erzeugung des PDF-Kartenatlas
enthält das QGIS-Projekt verschiedene, erforderliche Layer, die zudem verschiedene Styles aufweisen.

Es gibt folgende Layergruppen:

* **Radwegweiser** mit den für das Plugin wichtigen Layern
* **Sonstiges** mit weiteren Layern z.B. der landesweiten Radwegweisung 
* **Radwegenetz komplett (WMS):** Layer zum Radwege- bzw. Knotenpunktnetz 
* **Karten** mit Hintergrundkarten für die Bearbeitung bzw. die PDF-Atlaserzeugung 

.. figure:: img/projekt_layer1.png
    :figclass: align-center 
    :width: 60%

    Layer in der QGIS-Projektvorlage

Gruppe Radwegweiser
;;;;;;;;;;;;;;;;;;;;;;;

Der Layer **Wegweiser (alle)** zeigt alle Wegweiser an den Pfosten an.
Er nutzt dazu eine orangene Farbe und je nach Wegweisertyp ein unterschiedliches Schriftzeichen,
das an der Pfostenposition gemäß Richtung und Drehung des Wegweiser platziert wird.

.. figure:: img/wegweisertyp_font_char1.png
    :figclass: align-center 
    :width: 50%

    Schriftzeichen für Pfeil- (links) bzw. Tabellenwegweiser (rechts)

Der Layer **Wegweiser (Auswahl)** wird in der Layerreihenfolge darüber gezeichnet.
Er zeigt den aktuell im Reiter Wegweiser (Plugin) ausgewählten Wegweiser rot hervorgehoben an.
Zudem wird er mit der Wegweiser-ID beschriftet. 

Der Layer **Pfosten (Auswahl)** ist nur für die PDF-Erzeugung bedeutsam und ist in der Regel unsichtbar.
Als sog. **Abdeckungslayer** nutzt der Atlas diesen Layer, um für jeden Pfosten (des Knotens) 
die Inhalte im Drucklayout zu aktualisieren. Dazu nutzt er eine komplexe Datenbanksicht, 
die HTML-Quellcode zu Wegweisergrafiken und Fotos ausgibt. 

.. hint:: Die mit **Auswahl** bezeichneten Layer werden bei jedem Wechsel im Plugin neu gefiltert.
          Bei Auswahl eines Pfostens im Plugin wird der Filter von **Pfosten (Auswahl)** neu gesetzt,
          bei Auswahl eines Wegweisers der Filter von **Wegweiser (Auswahl)**. 
          Der aktuelle Filter des Layers kann als Tooltip des Filter-Icons abgefragt werden.

          Der Layer **Pfosten-PDF (Alle)** zeigt die gleichen Daten an wie **Pfosten (Auswahl)**,
          ist aber nicht gefiltert.

Der Layer **Pfosten (Editieren)** verweist auf die reinen Pfostendaten und kann genutzt werden, 
um Pfosten neu zu erstellen oder um sie zu verschieben. Schließlich kann der Layer **Knoten** 
genutzt werden, um Knoten neu zu erstellen oder sie zu verschieben. 

.. hint:: 
    Um einen Knoten oder Pfosten zu löschen, nutzen Sie bitte die  :ref:`entsprechende Funktion 
    im Plugin. <knoten_loeschen>`.

Gruppe Sonstiges
;;;;;;;;;;;;;;;;;;;;;;;

Diese Layergruppe enthält die Punktdaten zu den Wegweisern des Radverkehrsnetz NRW
als WMS-Layer **Wegweiser Radverkehrsnetz NRW** (Rasterdaten).
Dieser Layer kann genutzt werden, um die bisherige Beschilderung abzufragen oder auf Aktualität zu prüfen. 

.. hint:: Die Daten werden etwa vierteljährlich über die Schnittstelle des Schilderkatasters
          des Landes NRW in der Geodatenbank aktualisiert.

Gruppe Radwegenetz komplett (WMS)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Die Layergruppe enthält verschiedene Visualisierungen von Eigenschaften des **Radwegenetzes**
(Radroutenspeicher) sowie das **Knotenpunktnetz**.

Dieses kann z.B. vor der PDF-Erzeugung sichtbar geschaltet werden, um das Knotenpunktnetz 
mit in der PDF-Karte darzustellen.

Gruppe Karten
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

In dieser Layergruppe finden sich verschiedene Kartengrundlagen, die dem Anwender eine bessere
Bearbeitung von Knoten oder Pfosten erlauben bzw. die als Basiskarten im PDF-Atlas verwendet werden. 

Es handelt sich um:

* die amtliche Basiskarte in schwarz-weiss-Darstellung **ABK SW**
* deren Vorgänger (bis 2016) **DGK5 1937-2016 Grundriss**
* das farbige digitale Orthophoto des Landes NRW **DOP Farbe (NRW)**
* das neue Stadtplanwerk Ruhr in "light"-Darstellung **SPW2 light**
* sowie das farbige digitale Orthophoto (DOP) der Metropole Ruhr

.. hint:: Die **Layergruppe Karten** ist auf "gegenseitig ausschließende Gruppe" eingestellt.
          Daher ist immer nur ein Layer dieser Gruppe sichtbar. Beim Wechsel werden
          andere Layer der Gruppe automatisch auf unsichtbar gestellt.


Kartenthemen
;;;;;;;;;;;;;

In QGIS kann mit sog. **Kartenthemen** eine bestimmte Sichtbarkeit und Visualisierung (Stil) 
von Layern unter einer Bezeichnung abgespeichert werden. 

Es gibt im QGIS-Projekt die Themen:

* **uebersichtskarte**, im Drucklayout genutzt von der Übersichtskarte (1:50.000) 
* **detailkarte**, im Drucklayout genutzt von der Detailkarte (1:500) und
* **arbeitskarte** zum Zurückstellen der Einstellungen für die normale Bearbeitung in QGIS.

Folgende Layer sind im jeweiligen Thema sichtbar geschaltet: 

* **uebersichtskarte**: Wegweiser (Auswahl), SPW2 light
* **detailkarte**: Wegweiser (Auswahl), DOP
* **arbeitskarte**: Wegweiser (Auswahl), Wegweiser (Alle), Radwegenetz mit Routenzuordnung, SPW2 light

.. figure:: img/kartenthemen1.png
    :figclass: align-center 
    :width: 60%

    Umschalten zwischen Kartenthemen
    
In den Kartenthemen "detailkarte" und "uebersichtskarte" wird der Layer **Wegweiser (Auswahl)** 
auf den Stil **atlas** eingestellt. Dieser filtert die Wegweiser auf den aktuell im Atlas 
auszugebenden Pfosten (mittels @atlas_featureid). 
Im Kartenthema "arbeitskarte" wird der Layer wieder auf den Stil **default** eingestellt. 
Eine Filterung erfolgt hier über den Layer und nicht über eine Regel im Layerstil.
