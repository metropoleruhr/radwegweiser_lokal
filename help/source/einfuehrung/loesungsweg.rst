.. _loesungsweg:

Lösungsweg
###########

Die Auswertung und Bearbeitung von Geodaten erfolgt in der öffentlichen Verwaltung häufig mit dem 
Geoinformationssystem `QGIS <https://qgis.org/de/site/>`_. 
Dieses leistungsfähige, quelloffene und lizenzkostenfreie **Geoinformationssystem** ermöglicht es,
mittels Programmerweiterungen (sog. Plugins) in vielen Fällen proprietäre Fachanwendungen abzulösen.

Das Plugin **Radwegweiser (lokal)** hat u.a. folgende Funktionen:

* Verwaltung von :ref:`Knoten <begriff_knoten>`, :ref:`Pfosten <begriff_pfosten>`, 
  :ref:`Wegweisern <begriff_wegweiser>` und detaillierten :ref:`Schildinhalten <begriff_schild>` 
  in übersichtlichen Bedienoberflächen
* Kartenlayer der Knoten, Pfosten, Wegweiser zur Visualisierung und Auswahl zu bearbeitender Objekte 
* Fotodokumentation des Wegweiserbestandes
* Erzeugung der Wegweisergrafik mittels QML (Qt Modeling Language) und QtQuick
* Speicherung der Grafikdateien zu Wegweisern, Knotenpunkt- und Routeneinschüben sowie Fotos im lokalen Dateisystem
* Datenhaltung in einer SpatiaLite-Geodatenbank (GeoPackage)
* Erzeugung eines PDF-Katasterblattes zu allen Pfosten eines Knotens



