.. _ausgangslage:

Ausgangslage
##############

Die `Hinweise zur wegweisenden Beschilderung für den Radverkehr in NRW (HBR NRW) <https://www.radverkehrsnetz.nrw.de/rvn_hbr.asp>`_ 
geben technische Hinweise für eine einheitliche Beschilderung des Radnetz-NRW. 

Der Regionalverband Ruhr hat für die Dokumentation der von ihm unterhaltenen Wegweiser eine Software auf Basis
des freien Geoinformationssystems QGIS entwickelt. Auf vielfachen Wunsch im Geonetzwerk.metropoleRuhr wurde
diese Software soweit modifiziert, dass sie als Einzelplatz-Lösung ohne weitere Backend-Voraussetzungen 
(z.B. Datenbankserver) nutzbar ist.

Dies hat folgende Vorteile:

* die Kommunen können ihr radwegweisende Beschilderung ohne weitere Softwarekosten HBR NRW-konform dokumentieren

* PDF-Katasterblätter können so einfach erzeugt und dem landesweiten Kataster zur Verfügung gestellt werden

* die Datenbankstruktur und Software sind quelloffen und anpassbar.



