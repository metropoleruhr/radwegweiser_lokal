.. _gui:


Bedienoberfläche
*************************

Das Bedienfeld "Radwegweiser Metropole Ruhr" ist die zentrale Bedienoberfläche des Katasters.

Es unterteilt sich grob in folgende Bereiche:

* Einen **Reiter Knoten** zur Auflistung und Bearbeitung von Knoteneigenschaften
* einen Reiter **Pfosten** zur komplexen Berabeitung von Pfosten und daran befindlichen Wegweisern
  und deren Schildinhalten
* einen Reiter **Filter** zum schnellen Auffinden von Pfosten (oder Knoten) mit bestimmten Eigenschaften
* einen Reiter **Einstellungen** zum Einstellen z.B. von Verzeichnispfaden 

  .. figure:: img/reiter_pfosten.png
       :figclass: align-center
       :scale: 80%
       
       Bedienfeld *Radwegweiser Metropole Ruhr*

.. hint:: Im Regelfall beschränkt sich die Bearbeitung auf den **Reiter Pfosten**.


Im folgenden werden die Bedienelemente näher erläutert.

.. toctree::
   :maxdepth: 1
   
   gui_knoten.rst
   gui_pfosten.rst
   gui_wegweiser.rst
   gui_fotos.rst
   gui_filter.rst
   gui_einstellungen.rst
