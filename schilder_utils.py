# -*- coding: utf-8 -*-

from __future__ import print_function
from ast import And
from builtins import str
from builtins import zip
from builtins import range
from qgis.PyQt.QtCore import Qt, QObject, pyqtSignal, QSettings, QVariant, QRegExp, QDate
from qgis.PyQt.QtGui import QStandardItemModel, QStandardItem, QIcon, QPixmap, QRegExpValidator

from PyQt5.QtWidgets import QApplication, QFileDialog, QMessageBox, QComboBox, QHBoxLayout, QLabel, QLineEdit, QDateEdit, QCheckBox, QPushButton, QToolButton, QSpacerItem, QSizePolicy

from qgis.core import Qgis, QgsProject, QgsMapLayerType, QgsMessageLog, QgsProject, QgsVectorLayer, QgsRasterLayer, QgsGeometry, QgsPointXY, QgsPoint, QgsRectangle, QgsDataSourceUri, \
    QgsVectorFileWriter, QgsSymbol, QgsRuleBasedRenderer,  QgsFeature, QgsFeatureRequest, QgsSpatialIndex, QgsCoordinateReferenceSystem, \
    QgsPointLocator, QgsRenderContext, QgsWkbTypes, QgsApplication, QgsAuthManager, QgsAuthMethodConfig
from qgis.gui import QgsVertexMarker, QgsRubberBand, QgsMessageBar, QgsMapToolEmitPoint

from PyQt5.QtSql import QSqlTableModel

import math
import sqlite3



# QML_STRING2 = """
# import QtQuick 2.0
# import QtQuick.Shapes 1.15

# Item {
#     width: %(item_width)s; height: %(item_height)s
#     Column {
#         anchors.horizontalCenter: parent.horizontalCenter
#         anchors.verticalCenter: parent.verticalCenter
#         spacing: 3
# """

# für grabToImage muss die zeichnung links oben ausgerichtet sein:

QML_STRING2 = """
import QtQuick 2.0
import QtQuick.Shapes 1.15

Item {
    width: %(item_width)s; height: %(item_height)s
    Column {
        anchors.left: parent.left
        anchors.top: parent.top
        spacing: 3
"""



TAB_WW = """
        Rectangle { color: "white"; radius: 2.0; width: 500; height: 125
                    border.color: "gray"; border.width: 1
                
            Rectangle { color: "white"; radius: 6.0; x: 5; y: 5; width: 490; height: 115
                        border.color: %(farbe)s; border.width: 3
            }
"""

ZWISCHEN_WW = """
        Rectangle { color: "white"; radius: 2.0; width: 80; height: 80
                    border.color: "gray"; border.width: 1
                
            Rectangle { color: "white"; radius: 6.0; x: 5; y: 5; width: 70; height: 70
                        border.color: %(farbe)s; border.width: 3
            }
"""

PFEIL_WW_RECHTS = """
        Rectangle { color: "white"; radius: 2.0; width: 500; height: 125
                    border.color: "gray"; border.width: 1
            Shape {
                ShapePath {
                    strokeWidth: 2
                    strokeColor: %(farbe)s
                    dashPattern: [ 1, 4 ]
                    startX: 5; startY: 5
                    PathLine { x: 420; y: 5 }
                    PathLine { x: 490; y: 62.5 }
                    PathLine { x: 420; y: 120 }
                    PathLine { x: 5; y: 120 }
                    PathLine { x: 5; y: 5 }
                }
                ShapePath {
                    strokeWidth: 2
                    strokeColor: %(farbe)s
                    fillColor: %(farbe)s
                    capStyle: ShapePath.FlatCap
                    joinStyle: ShpePath.BevelJoin
                    startX: 420; startY: 5
                    PathLine { x: 445; y: 5 }
                    PathLine { x: 495; y: 62.5 }
                    PathLine { x: 445; y: 120 }
                    PathLine { x: 420; y: 120 }
                    PathLine { x: 470; y: 62.5 }
                    PathLine { x: 420; y: 5 }
                }
            }

"""

PFEIL_WW_LINKS = """
        Rectangle { color: "white"; radius: 2.0; width: 500; height: 125
                    border.color: "gray"; border.width: 1
            Shape {
                ShapePath {
                    strokeWidth: 2
                    strokeColor: %(farbe)s
                    dashPattern: [ 1, 4 ]
                    startX: 5; startY: 62.5
                    PathLine { x: 80; y: 5 }
                    PathLine { x: 495; y: 5 }
                    PathLine { x: 495; y: 120 }
                    PathLine { x: 80; y: 120 }
                    PathLine { x: 5; y: 62.5 }
                }
                ShapePath {
                    strokeWidth: 2
                    strokeColor: %(farbe)s
                    fillColor: %(farbe)s
                    capStyle: ShapePath.FlatCap
                    joinStyle: ShpePath.BevelJoin
                    startX: 80; startY: 5
                    PathLine { x: 55; y: 5 }
                    PathLine { x: 5; y: 62.5 }
                    PathLine { x: 55; y: 120 }
                    PathLine { x: 80; y: 120 }
                    PathLine { x: 30; y: 62.5 }
                    PathLine { x: 80; y: 5 }
                }
            }

"""


# Inhalt mit Pfeil links oder geradeaus
TABELLENWW_INHALT_LINKS = """
    Text{ id: fern%(schild)s; text: "%(fernziel_zp1)s%(fernziel_zp2)s %(fernziel)s %(fernziel_sp1)s%(fernziel_sp2)s "; color: %(farbe)s; font.family: "HBR NRW Eng"; 
    font.pointSize: 30; fontSizeMode: Text.HorizontalFit; width: 343; height: 50; verticalAlignment: Text.AlignVCenter; x: 80; y:10}
    Text{ textFormat: Text.RichText; text: "%(fernziel_entfernung)s" ; horizontalAlignment: Text.AlignRight; color: %(farbe)s; font.family: "HBR NRW Eng"; font.pointSize: 30; width: 75; x: 400; y: 10}
    Text{ id: nah%(schild)s; text: "%(nahziel_zp1)s%(nahziel_zp2)s %(nahziel)s %(nahziel_sp1)s%(nahziel_sp2)s "; color: %(farbe)s; font.family: "HBR NRW Eng"; 
    font.pointSize: 30; fontSizeMode: Text.HorizontalFit; width: 342; height: 50; verticalAlignment: Text.AlignVCenter; x: 80; y: 60}
    Text{ textFormat: Text.RichText; text: "%(nahziel_entfernung)s" ; horizontalAlignment: Text.AlignRight; color: %(farbe)s; font.family: "HBR NRW Eng"; font.pointSize: 30; width: 75; x: 400; y: 60}
    Text{ text: "%(pfeil_zeichen)s" ; color: %(farbe)s; font.family: "HBR NRW Eng"; font.pointSize: 40; x: 14; y: 5}
    Text{ text: "%(radsymbol)s" ; color: %(farbe)s; font.family: "HBR NRW Eng"; font.pointSize: 30; x: 22; y: 60}
    """

# Inhalt mit Pfeil rechts
    # Text{ text: "%(fernziel)s" ; color: %(farbe)s; font.family: "HBR NRW Eng"; font.pointSize: 30; x: 90; y:10}

# Done: Leerzeichen am Anfang von %(fernziel_zp1)s%(fernziel_zp2)s %(fernziel)s %(fernziel_sp1)s%(fernziel_sp2)s trimmen
# fontSizeMode: Text.Fit - font.pointSize: 30;
# transform: Scale {xScale: 0.75; yScale: 1.0};  funktioniert nicht
TABELLENWW_INHALT_RECHTS = """
    Text{ id: fern%(schild)s; text: "%(fernziel_zp1)s%(fernziel_zp2)s %(fernziel)s %(fernziel_sp1)s%(fernziel_sp2)s "; color: %(farbe)s; font.family: "HBR NRW Eng"; 
    font.pointSize: 30; fontSizeMode: Text.HorizontalFit; width: 343; height: 50; verticalAlignment: Text.AlignVCenter; x: 20; y:10}
    Text{ textFormat: Text.RichText; text: "%(fernziel_entfernung)s" ; horizontalAlignment: Text.AlignRight; color: %(farbe)s; font.family: "HBR NRW Eng"; font.pointSize: 30; width: 75; x: 350; y: 10}
    Text{ id: nah%(schild)s; text: "%(nahziel_zp1)s%(nahziel_zp2)s %(nahziel)s %(nahziel_sp1)s%(nahziel_sp2)s "; color: %(farbe)s; font.family: "HBR NRW Eng"; 
    font.pointSize: 30; fontSizeMode: Text.HorizontalFit; width: 343; height: 50; verticalAlignment: Text.AlignVCenter; x: 20; y: 60}
    Text{ textFormat: Text.RichText; text: "%(nahziel_entfernung)s" ; horizontalAlignment: Text.AlignRight; color: %(farbe)s; font.family: "HBR NRW Eng"; font.pointSize: 30; width: 75; x: 350; y: 60}
    Text{ text: "%(pfeil_zeichen)s" ; color: %(farbe)s; font.family: "HBR NRW Eng"; font.pointSize: 40; width: 50; x: 440; y: 5}
    Text{ text: "%(radsymbol)s" ; color: %(farbe)s; font.family: "HBR NRW Eng"; font.pointSize: 30; width: 60; x: 440; y: 60}
    """

# PfeilWW links
PFEILWW_INHALT_LINKS = """
    Text{ id: fern%(schild)s; text: "%(fernziel_zp1)s%(fernziel_zp2)s %(fernziel)s %(fernziel_sp1)s%(fernziel_sp2)s "; color: %(farbe)s; font.family: "HBR NRW Eng"; 
    font.pointSize: 30; fontSizeMode: Text.HorizontalFit; width: 330; height: 50; verticalAlignment: Text.AlignVCenter; x: 110; y:10}
    Text{ textFormat: Text.RichText; text: "%(fernziel_entfernung)s" ; horizontalAlignment: Text.AlignRight; color: %(farbe)s; font.family: "HBR NRW Eng"; font.pointSize: 30; width: 75; x: 405; y: 10}
    Text{ id: nah%(schild)s; text: "%(nahziel_zp1)s%(nahziel_zp2)s %(nahziel)s %(nahziel_sp1)s%(nahziel_sp2)s "; color: %(farbe)s; font.family: "HBR NRW Eng"; 
    font.pointSize: 30; fontSizeMode: Text.HorizontalFit; width: 330; height: 50; verticalAlignment: Text.AlignVCenter; x: 110; y: 60}
    Text{ textFormat: Text.RichText; text: "%(nahziel_entfernung)s" ; horizontalAlignment: Text.AlignRight; color: %(farbe)s; font.family: "HBR NRW Eng"; font.pointSize: 30; width: 75; x: 405; y: 60}
    Text{ text: "%(radsymbol)s" ; color: %(farbe)s; font.family: "HBR NRW Eng"; font.pointSize: 30; x: 45; y: 35}
    """

# PfeilWW rechts
PFEILWW_INHALT_RECHTS = """
    Text{ id: fern%(schild)s; text: "%(fernziel_zp1)s%(fernziel_zp2)s %(fernziel)s %(fernziel_sp1)s%(fernziel_sp2)s "; color: %(farbe)s; font.family: "HBR NRW Eng"; 
    font.pointSize: 30; fontSizeMode: Text.HorizontalFit; width: 330; height: 50; verticalAlignment: Text.AlignVCenter; x: 20; y:10}
    Text{ textFormat: Text.RichText; text: "%(fernziel_entfernung)s" ; horizontalAlignment: Text.AlignRight; color: %(farbe)s; font.family: "HBR NRW Eng"; font.pointSize: 30; width: 75; x: 330; y: 10}
    Text{ id: nah%(schild)s; text: "%(nahziel_zp1)s%(nahziel_zp2)s %(nahziel)s %(nahziel_sp1)s%(nahziel_sp2)s "; color: %(farbe)s; font.family: "HBR NRW Eng"; 
    font.pointSize: 30; fontSizeMode: Text.HorizontalFit; width: 330; height: 50; verticalAlignment: Text.AlignVCenter; x: 20; y: 60}
    Text{ textFormat: Text.RichText; text: "%(nahziel_entfernung)s" ; horizontalAlignment: Text.AlignRight; color: %(farbe)s; font.family: "HBR NRW Eng"; font.pointSize: 30; width: 75; x: 330; y: 60}
    Text{ text: "%(radsymbol)s" ; color: %(farbe)s; font.family: "HBR NRW Eng"; font.pointSize: 30; width: 60; x: 410; y: 35}
    """

ZWISCHENWW_INHALT = """
    Text{ text: "%(pfeil_zeichen)s" ; color: %(farbe)s; font.family: "HBR NRW Eng"; font.pointSize: 30; anchors.horizontalCenter: parent.horizontalCenter; y: 0}
    Text{ text: "%(radsymbol)s" ; horizontalAlignment: Text.AlignCenter; color: %(farbe)s; font.family: "HBR NRW Eng"; font.pointSize: 22; anchors.horizontalCenter: parent.horizontalCenter; y: 40}
    """


def addProzentLabel(props, wegweiser):
    # QgsMessageLog.logMessage('addProzentLabel' , 'Radwegweiser', Qgis.Info)
    fernOderNah = 0
    # #### ggf. Prozentangabe für das Nahziel 
    # Streckenpiktogramm Steigung/Gefälle untersuchen
    steigungGefaelleFirst = False   # ist Steigung/Gefälle erstes Streckenpiktogramm
    isSteigung = 0                  # ist das Piktogramm Steigung 1 oder Gefälle 2 oder etwas anderes 0
    otherSecond = False              # gibt es ein anderes zweites Streckenpiktogramm
    prozentX = 32
    prozentY = 15
    prozentAngle = -35
    if props["fernziel_sp1_bezeichnung"] == 'Steigung':
        steigungGefaelleFirst = True
        isSteigung = 1
    elif props["fernziel_sp1_bezeichnung"] == 'Gefälle':
        steigungGefaelleFirst = True
        isSteigung = 2
    elif props["fernziel_sp1_bezeichnung"] == 'kein':
        steigungGefaelleFirst = False
        isSteigung = 0

    if props["fernziel_sp2_bezeichnung"] == 'Steigung':
        steigungGefaelleFirst = False
        isSteigung = 1
    elif props["fernziel_sp2_bezeichnung"] == 'Gefälle':
        steigungGefaelleFirst = False
        isSteigung = 2
    elif props["fernziel_sp2_bezeichnung"] == 'kein':
        steigungGefaelleFirst = False
        if isSteigung < 1: # wenn schon gesetzt nicht mehr auf 0 setzen
            isSteigung = 0
    else:
        otherSecond = True
    
    if props["pfeil_links"]:
        if isSteigung == 1: # Piktogramm Steigung
            prozentAngle = -35
            prozentY = 15
            if otherSecond:
                prozentX = 0
            else:
                prozentX = 32
        elif isSteigung == 2: # Piktogramm Gefälle
            prozentAngle = 35
            prozentY = 4
            if otherSecond:
                prozentX = 40
            else:
                prozentX = 74
        if props["wegweisertyp_id"] in ('Pfeilwegweiser','Pfeilwegweiser (gedreht)'):
            prozentX += 30                
    else:   # pfeil_rechts
        if isSteigung == 1: # Piktogramm Steigung
            prozentAngle = -35
            prozentY = 15
            if otherSecond:
                prozentX = -60
            else:
                prozentX = -26
        elif isSteigung == 2: # Piktogramm Gefälle
            prozentAngle = 35
            prozentY = 4
            if otherSecond:
                prozentX = -20
            else:
                prozentX = 16


    # ermitteln, ob fern- bzw. nahziel > 0 wenn fernziel_steigung_prozent > 0 danach wenn nahziel_steigung_prozent > 0 ebenfalls ergänzen
    # bei jedem wegweisertyp steigungText ergänzen: funktion ergaenzeSteigungProzent
    if isSteigung > 0:
        steigungText = """
                    Text{{ text: "{0}%"; color: {5}; font.family: "HBR NRW Eng"; 
                    font.pointSize: 15; fontSizeMode: Text.FixedSize; width: 50; height: 50; verticalAlignment: Text.AlignVCenter; x: fern{1}.paintedWidth+{2}; y:{3};
                    transform: Rotation {{ origin.x: text.width; origin.y: 0; angle: {4} }} }}
                    """
        wegweiser += steigungText.format(str(props["fernziel_steigung_prozent"]), props["schild"], prozentX, prozentY, prozentAngle, props["farbe"]) 

    # #### ggf. Prozentangabe für das Nahziel 
    # wenn ein Streckenpiktogramm Steigung/Gefälle untersuchen
    steigungGefaelleFirst = False   # ist Steigung/Gefälle erstes Streckenpiktogramm
    isSteigung = 0                  # ist das Piktogramm Steigung 1 oder Gefälle 2 oder etwas anderes 0
    otherSecond = False              # gibt es ein anderes zweites Streckenpiktogramm
    prozentX = 32
    prozentY = 15
    prozentAngle = -35
    if props["nahziel_sp1_bezeichnung"] == 'Steigung':
        steigungGefaelleFirst = True
        isSteigung = 1
    elif props["nahziel_sp1_bezeichnung"] == 'Gefälle':
        steigungGefaelleFirst = True
        isSteigung = 2
    elif props["nahziel_sp1_bezeichnung"] == 'kein':
        steigungGefaelleFirst = False
        isSteigung = 0

    if props["nahziel_sp2_bezeichnung"] == 'Steigung':
        steigungGefaelleFirst = False
        isSteigung = 1
    elif props["nahziel_sp2_bezeichnung"] == 'Gefälle':
        steigungGefaelleFirst = False
        isSteigung = 2
    elif props["nahziel_sp2_bezeichnung"] == 'kein':
        steigungGefaelleFirst = False
        if isSteigung < 1: # wenn schon gesetzt nicht mehr auf 0 setzen
            isSteigung = 0
    else:
        otherSecond = True

    if props["pfeil_links"]:
        if isSteigung == 1: # Piktogramm Steigung
            prozentAngle = -35
            prozentY = 64
            if otherSecond:
                prozentX = -8
            else:
                prozentX = 34
        elif isSteigung == 2: # Piktogramm Gefälle
            prozentAngle = 35
            prozentY = 54
            if otherSecond:
                prozentX = 42
            else:
                prozentX = 74

        if props["wegweisertyp_id"] in ('Pfeilwegweiser','Pfeilwegweiser (gedreht)'):
            prozentX += 30

    else: # pfeil_rechts
        if isSteigung == 1: # Piktogramm Steigung
            prozentAngle = -44
            prozentY = 70
            if otherSecond:
                prozentX = -72
            else:
                prozentX = -30
        elif isSteigung == 2: # Piktogramm Gefälle
            prozentAngle = 35
            prozentY = 54
            if otherSecond:
                prozentX = -20
            else:
                prozentX = 14


    if isSteigung > 0:
        steigungText = """
                    Text{{ text: "{0}%"; color: {5}; font.family: "HBR NRW Eng"; 
                    font.pointSize: 15; fontSizeMode: Text.FixedSize; width: 50; height: 50; verticalAlignment: Text.AlignVCenter; x: nah{1}.paintedWidth+{2}; y:{3};
                    transform: Rotation {{ origin.x: text.width; origin.y: 0; angle: {4} }} }}
                    """
        wegweiser += steigungText.format(str(props["nahziel_steigung_prozent"]), props["schild"], prozentX, prozentY, prozentAngle, props["farbe"]) 
    
    return wegweiser


def addSchild(wegweiser, props, item_width, item_height):
    # QgsMessageLog.logMessage('addSchild %s %s' % (props["schild"], props["wegweisertyp_id"]), 'Radwegweiser', Qgis.Info)
    if wegweiser == "":
        wegweiser += QML_STRING2 % {"item_width": item_width, "item_height": item_height}
    # Rectangle und Inhalte erstellen
    if props["wegweisertyp_id"] == 'Tabellenwegweiser': # Tabellenwegweiser
        # QgsMessageLog.logMessage('addSchild Tabellenwegweiser', 'Radwegweiser', Qgis.Info)
        wegweiser += TAB_WW % props
        if props["pfeil_links"]:
            wegweiser += TABELLENWW_INHALT_LINKS % props
        else:
            wegweiser += TABELLENWW_INHALT_RECHTS % props
        if props["fernziel_steigung_prozent"] > 0 or props["nahziel_steigung_prozent"] > 0 :
            wegweiser = addProzentLabel(props, wegweiser)
            # QgsMessageLog.logMessage('addSchild wegweiser ist %s' % wegweiser, 'Radwegweiser', Qgis.Info)
    elif props["wegweisertyp_id"] in ('Zwischenwegweiser', 'Zwischenwegweiser mit Themenroute'): # Zwischenwegweiser
        # QgsMessageLog.logMessage('addSchild Zwischenwegweiser', 'Radwegweiser', Qgis.Info)
        wegweiser += ZWISCHEN_WW % props
        wegweiser += ZWISCHENWW_INHALT % props
    elif props["wegweisertyp_id"] in ('Pfeilwegweiser','Pfeilwegweiser (gedreht)'): # Pfeilwegweiser
        if props["pfeil_links"]:
            # QgsMessageLog.logMessage('addSchild Pfeilwegweiser links', 'Radwegweiser', Qgis.Info)
            wegweiser += PFEIL_WW_LINKS % props
            wegweiser += PFEILWW_INHALT_LINKS % props
        else:
            # QgsMessageLog.logMessage('addSchild Pfeilwegweiser rechts', 'Radwegweiser', Qgis.Info)
            wegweiser += PFEIL_WW_RECHTS % props
            wegweiser += PFEILWW_INHALT_RECHTS % props

        if props["fernziel_steigung_prozent"] > 0 or props["nahziel_steigung_prozent"] > 0 :
            wegweiser = addProzentLabel(props, wegweiser)
            # QgsMessageLog.logMessage('addSchild wegweiser ist %s' % wegweiser, 'Radwegweiser', Qgis.Info)

    else:
        wegweiser += TAB_WW % props
        if props["pfeil_links"]:
            wegweiser += TABELLENWW_INHALT_LINKS % props
        else:
            wegweiser += TABELLENWW_INHALT_RECHTS % props
        if props["fernziel_steigung_prozent"] > 0 or props["nahziel_steigung_prozent"] > 0 :
            wegweiser = addProzentLabel(props, wegweiser)

    wegweiser += "}" # Rectangle schliessen

    

    # QgsMessageLog.logMessage('addSchild %s' % wegweiser, 'Radwegweiser', Qgis.Info)
    return wegweiser

def getWegweiserSize(self):
    QgsMessageLog.logMessage('getWegweiserSize', 'Radwegweiser lokal', Qgis.Info)
    inhalte_anzahl = self.wegweiserInhaltTableModel.rowCount()
    item_width = 500 
    item_height = ((math.ceil(inhalte_anzahl/2)) * 125) + 5 
    return (item_width, item_height)





def getPfeilZeichen(self, pfeilart, richtung):
        # QgsMessageLog.logMessage('getPfeilZeichen (%s) %s' % (pfeilart, richtung), 'Radwegweiser lokal', Qgis.Info)
        pfeil_zeichen = '?'
        pfeil_links = True
        if pfeilart is None or pfeilart == ' ': # Pfeilwegweiser
            if richtung > 4:
                pfeil_links = True
            else:
                pfeil_links = False
        else:
            pfeil_zeichen = self.getZeichenfuerPiktogramm(pfeilart)
            if pfeilart in ('geradeaus'):
                pfeil_links = True
            elif pfeilart in ('links'):
                pfeil_links = True
            elif pfeilart in ('rechts'):
                pfeil_links = False
            elif pfeilart in ('schräg links'):
                pfeil_links = True
            elif pfeilart in ('schräg rechts'):
                pfeil_links = False
            elif pfeilart in ('Versatz links'):
                pfeil_links = True
            elif pfeilart in ('Versatz rechts'):
                pfeil_links = False
            elif pfeilart == 'gebogen links':
                pfeil_links = True
            elif pfeilart == 'gebogen rechts':
                pfeil_links = False
            elif pfeilart == 'wenden links':
                pfeil_links = True
            elif pfeilart == 'wenden rechts':
                pfeil_links = False
            elif pfeilart == 'nach Kreis geradeaus':
                pfeil_links = True
            elif pfeilart == 'im Kreis 1.Abzweig':
                pfeil_links = False
            elif pfeilart == 'im Kreis 3.Abzweig':
                pfeil_links = True
            else:
                pfeil_zeichen = '?'

        return (pfeil_zeichen, pfeil_links)

